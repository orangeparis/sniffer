# build snaps with docker snapcraft :

# STATUS: NOT WORKING

see: https://snapcraft.io/docs/t/creating-docker-images-for-snapcraft/11739







## create a custom snapcraft docker image for core 18 : snapcore18:stable

    docker build --no-cache -f stable.Dockerfile --label snapcore18 --tag snapcore18:stable --network host .



## Running a build

After creating  the snapcraft Docker image: snapcore:stable, 
return to the root directory of the project containing your snapcraft.yaml and run snapcraft:

    $ docker run -v "$PWD":/build -w /build  snapcore18:stable snapcraft

## checks the build

When the snap build completes successfully, 
you will find a .snap file in the current directory. 
You can inspect its contents to ensure it contains all of your application’s assets:

    $ unsquashfs -l *.snap
    

