package _sniffer

import "strings"

/*

	helper function


*/

// MacVendor some livebox vendors for MacAddress
var MacVendor = map[string]string{
	"C0D044": "SAGEMCOM",
	"904D4A": "SAGEMCOM",
	"A01B29": "SAGEMCOM",
	"84A1D1": "SAGEMCOM",
	"7894B4": "SERCOM",

	"A47B2C": "NOKIA",
	"3C6104": "JUNIPER",
}

// GetVendor return the Vendor for the mac address
func GetVendor(mac string) (vendor string, ok bool) {

	// canonical vendor info from mac address
	v := strings.TrimSpace(mac)
	v = strings.ReplaceAll(v, ":", "")
	v = strings.ToUpper(v)
	if len(v) >= 6 {
		v = v[:6]
		vendor, ok = MacVendor[v]
	} else {
		ok = false
	}

	return vendor, ok
}

// IsLivebox : is this mac addrress belongs to a livebox ( "SAGEMCOM" or "SERCOM")
func IsLivebox(mac string) bool {
	vendor, ok := GetVendor(mac)
	if ok {
		if vendor == "SAGEMCOM" || vendor == "SERCOM" {
			return true
		}
	}
	return false

}
