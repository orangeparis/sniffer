package evs_test

/*
	testing ulid tools : store timestamp with lexicographically sortable keys


*/

import (
	"log"
	"testing"
	"time"

	"encoding/json"

	"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/dgraph-io/badger"
	"github.com/oklog/ulid"
)

type FakeEvent struct {
	Number    int // we also add a number to keep track of the ground-truth creation order
	Timestamp time.Time
	ID        ulid.ULID
}

func TestStulid(t *testing.T) {

	var nbEvents = 100000

	ts0 := time.Unix(1000000, 0)

	// reproducible entropy source
	entropy := evs.GetMonotonicEntropy(ts0)

	// sub-ms safe ULID generator
	//ulidSource := ulid.MustNew(ulid.Timestamp(ts0), entropy)

	// generate fake events that contain a ground-truth sorting order and a ULID
	var events []FakeEvent
	ts := ts0
	for i := 0; i < nbEvents; i++ {

		// create ulid with entropy
		id, err := ulid.New(ulid.Timestamp(ts), entropy)
		if err != nil {
			t.Fail()
			return
		}
		ev := FakeEvent{i, ts, id}
		events = append(events, ev)
		//time.Sleep(10 * time.Microsecond)
		ts = ts.Add(100 * time.Microsecond)
	}

	firstEvent := events[0].Timestamp
	lastEvent := events[len(events)-1].Timestamp
	timeRange := lastEvent.Sub(firstEvent)
	_ = lastEvent
	log.Printf(" time range of %d items is %s", nbEvents, timeRange)
	//elapsed := time.Since(firstEvent)
	//log.Printf("create %d items took %s", nbEvents, elapsed)

	// entropy.Shuffle(len(events), func(i, j int) {
	// 	events[i], events[j] = events[j], events[i]
	// })

	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir) // .badger.WithDir(tmpdir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)
	defer db.Close()

	// open a database transaction
	txn := db.NewTransaction(true)

	for _, e := range events {

		// serialize the event payload (to JSON for simplicity)
		eSerial, err := json.Marshal(e)
		if err != nil {
			panic(err)
		}

		// serialize the ULID to its binary form
		binID, err := e.ID.MarshalBinary()
		if err != nil {
			panic(err)
		}

		// add the insert operation to the transaction
		// and open a new transaction if this one is full
		if err := txn.Set(binID, []byte(eSerial)); err == badger.ErrTxnTooBig {
			if err := txn.Commit(); err != nil {
				panic(err)
			}
			txn = db.NewTransaction(true)
			if err := txn.Set(binID, []byte(eSerial)); err != nil {
				panic(err)
			}
		}
	}

	// flush the transaction
	if err := txn.Commit(); err != nil {
		panic(err)
	}

	// evaluate time to gathers keys
	var retrievedKeys []string
	_ = retrievedKeys
	if err := db.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		iterStart := time.Now()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			_ = k

			retrievedKeys = append(retrievedKeys, string(k))

		}
		elapsed := time.Since(iterStart)
		log.Printf("iterate keys with %d items took %s", nbEvents, elapsed)
		return nil
	}); err != nil {
		panic(err)
	}

	// evaluate time to gather time window keys
	// start at 5s and collect for 50 ms

	// search db for tsstart and collect key to ts stop
	tsStart := ts0.Add(6 * time.Second)
	//tsStop := tsStart.Add(50 * time.Millisecond)

	b0, b1, err := evs.UlidTimeBoundaries(tsStart, 50*time.Millisecond)
	if err != nil {
		t.Fail()
		return
	}
	log.Printf("boundary keys : %s -> %s\n", b0, b1)
	_ = b0
	_ = b1
	// // validate badger iteration order is equal to original creation order
	var collected []FakeEvent
	if err := db.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		iterStart := time.Now()

		// search first key
		it.Seek(b0.Bytes())
		first := true

		for it.Seek(b0.Bytes()); it.Valid(); it.Next() {
			item := it.Item()

			v := make([]byte, item.ValueSize())
			value, err := item.ValueCopy(v)
			if err != nil {
				panic(err)
			}

			// deserialize the fake events and store them
			var des FakeEvent
			err = json.Unmarshal(value, &des)
			if err != nil {
				panic(err)
			}
			collected = append(collected, des)

			if first {
				log.Printf("first key: n=%d\n", des.Number)
				first = false
			}

			//
			// test end of time window
			//

			// rebuild ulid from key bytes
			k := item.Key()
			x := ulid.ULID{}
			x.UnmarshalBinary(k)
			//fmt.Println(x.String())

			// check ulid stored
			id := des.ID.String()
			current := x.String()
			if current != id {
				log.Fatal("COHERENCE PROBLEM")
			}

			//fmt.Println("id: " + des.ID.String())

			if string(current) > b1.String() {
				// end of window
				log.Printf("last key: n=%d\n", des.Number)
				break
			}

		}
		elapsed := time.Since(iterStart)
		log.Printf("collecting %d items took %s", len(collected), elapsed)

		return nil
	}); err != nil {
		panic(err)
	}

}
