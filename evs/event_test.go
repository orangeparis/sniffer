package evs_test

/*
	testing event

*/

import (
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/oklog/ulid"
)

func TestEvent(t *testing.T) {

	// common base for entropy
	x := time.Unix(1000000, 0)
	entropy := ulid.Monotonic(rand.New(rand.NewSource(x.UnixNano())), 0)

	// data for a event
	tx := time.Unix(1000000, 0)
	data := []byte{}

	// direct computation of ulid
	u0 := ulid.MustNew(ulid.Timestamp(tx), entropy)
	_ = u0

	// compute event via lib
	e, err := evs.NewEvent(data, tx, entropy)
	if err != nil {
		t.Fail()
		return
	}
	// check it
	if e.ID != "0000XSNJG0MQJHBF4QX3A9X93K" {
		t.Fail()
		return
	}

	u2, _ := e.GetUlid()
	if u2.Time() != u0.Time() {
		t.Fail()
		return
	}

	// check retrieve timestamp from event
	tx2, _ := e.GetTimestamp()

	if tx2.String() != tx.String() {
		t.Fail()
		return
	}

	// create an event with nil timestamp
	tnil := time.Time{}
	e2, err := evs.NewEvent(data, tnil, entropy)
	if err != nil {
		t.Fail()
		return
	}
	_ = e2

	println("")
}

func TestErrorEvent(t *testing.T) {

	e1 := evs.NewErrorEvent("404", "End of File")

	valid, reason := e1.IsValid()
	if valid != false {
		t.Fail()
		return
	}
	_ = reason
	if reason != "!404" {
		t.Fail()
		return
	}

	e2 := evs.NewErrorEvent("", "")
	valid, reason = e2.IsValid()
	if valid != false {
		t.Fail()
		return
	}
	_ = reason
	if reason != "!" {
		t.Fail()
		return
	}

	e3 := evs.NewErrorEvent("INVALID-ULID", "")
	valid, reason = e3.IsValid()
	if valid != false {
		t.Fail()
		return
	}
	_ = reason
	if reason != "!INVALID-ULID" {
		t.Fail()
		return
	}

	e4, _ := evs.NewTimestamp([]byte{}, time.Now())
	valid, reason = e4.IsValid()
	if valid != true {
		t.Fail()
		return
	}

}
