package evs

import (
	"time"

	"encoding/json"

	"github.com/oklog/ulid"
)

//"bitbucket.org/orangeparis/sniffer/evs"

/*

	an event is composed of an ulid and data

*/

// EventErrCode special values for event ID to denote Errs
// a normal id is a ulid identier composed od 26 chars with no puntuation code
// so there is no conflict with these special error Id codes
var EventErrCode = map[string]string{
	"!400": "400  Bad Format",
	"!404": "404 Not Found",
	"!500": "500 Failed",
}

// EventErrData : format of a Event Data field when it is an error
type EventErrData struct {
	Code string
}

// Event : an opaque event for storage ID is a chronolicaly sortable
type Event struct {
	ID   string // an ulid id  eg 0000XSNQC8D56SJRAVX4EDS9NG or an err code begins with !
	Data []byte // opaque data , usually a json representation of an event
}

// NewEvent : create a new event with data , timestamp and entropy
//    if timestamp is Zero : compute tilme.Now
func NewEvent(data []byte, ts time.Time, entropy *ulid.MonotonicEntropy) (e Event, err error) {

	if ts.IsZero() {
		ts = time.Now()
	}
	u, err := ulid.New(ulid.Timestamp(ts), entropy)
	if err != nil {
		return e, err
	}
	e.ID = u.String()
	e.Data = data
	return e, err
}

// NewTimestamp : create a new event with no entropy
//    if timestamp is Zero : compute tilme.Now
func NewTimestamp(data []byte, ts time.Time) (e Event, err error) {

	if ts.IsZero() {
		ts = time.Now()
	}
	u, err := ulid.New(ulid.Timestamp(ts), nil)
	if err != nil {
		return e, err
	}
	e.ID = u.String()
	e.Data = data
	return e, err
}

// GetUlid : return the Ulid object  from the string ID
func (e *Event) GetUlid() (id ulid.ULID, err error) {
	id, err = ulid.Parse(e.ID)
	return id, err
}

// GetTimestamp return the timestamp object corresponding to the string ID
func (e *Event) GetTimestamp() (ts time.Time, err error) {

	id, err := ulid.Parse(e.ID)
	if err != nil {
		return ts, err
	}
	// get time in millisecond
	t := id.Time()
	// convert to a time object
	ts = ulid.Time(t)

	return ts, err
}

// IsValid : check if ID is a valid ULID ( can be empty or err code)
func (e *Event) IsValid() (valid bool, reason string) {
	if e.ID == "" {
		// not valid because empty
		return false, "Empty"
	}
	if e.ID[0] == '!' {
		return false, e.ID
	}
	if len(e.ID) != 26 {
		return false, "Invalid Ulid"
	}
	return true, ""

}

// NewErrorEvent create a Error Event
func NewErrorEvent(code string, reason string) (event Event) {
	event.ID = "!" + code
	data := EventErrData{Code: reason}
	event.Data, _ = json.Marshal(data)
	return event
}
