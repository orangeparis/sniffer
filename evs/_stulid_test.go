package evs

/*
	testing ulid tools : store timestamp with lexicographically sortable keys

	see: https://barkeywolf.consulting/posts/badger-event-store/
*/

import (
	"log"
	"math/rand"
	"testing"
	"time"

	"encoding/json"

	"github.com/dgraph-io/badger"
	"github.com/oklog/ulid"
)

type FakeEvent struct {
	Number    int // we also add a number to keep track of the ground-truth creation order
	Timestamp time.Time
	ID        ulid.ULID
}

func TestStulid(t *testing.T) {

	var nbEvents = 100000

	// reproducible entropy source
	entropy := rand.New(rand.NewSource(time.Unix(1000000, 0).UnixNano()))

	// sub-ms safe ULID generator
	ulidSource := NewMonotonicULIDsource(entropy)

	// generate fake events that contain a ground-truth sorting order and a ULID
	var events []FakeEvent
	for i := 0; i < nbEvents; i++ {
		now := time.Now()
		id, _ := ulidSource.New(now)
		ev := FakeEvent{i, now, id}
		events = append(events, ev)
		time.Sleep(10 * time.Microsecond)
	}

	firstEvent := events[0].Timestamp
	lastEvent := events[len(events)-1].Timestamp
	_ = lastEvent

	elapsed := time.Since(firstEvent)
	log.Printf("create %d items took %s", nbEvents, elapsed)

	entropy.Shuffle(len(events), func(i, j int) {
		events[i], events[j] = events[j], events[i]
	})

	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions
	opts.Dir = tmpDir
	opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)
	defer db.Close()

	// open a database transaction
	txn := db.NewTransaction(true)

	for _, e := range events {

		// serialize the event payload (to JSON for simplicity)
		eSerial, err := json.Marshal(e)
		if err != nil {
			panic(err)
		}

		// serialize the ULID to its binary form
		binID, err := e.ID.MarshalBinary()
		if err != nil {
			panic(err)
		}

		// add the insert operation to the transaction
		// and open a new transaction if this one is full
		if err := txn.Set(binID, []byte(eSerial)); err == badger.ErrTxnTooBig {
			if err := txn.Commit(nil); err != nil {
				panic(err)
			}
			txn = db.NewTransaction(true)
			if err := txn.Set(binID, []byte(eSerial)); err != nil {
				panic(err)
			}
		}
	}

	// flush the transaction
	if err := txn.Commit(nil); err != nil {
		panic(err)
	}

	// evaluate time to gathers keys
	var retrievedKeys []string
	_ = retrievedKeys
	if err := db.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		iterStart := time.Now()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			k := item.Key()
			_ = k

			//retrievedKeys = append(retrievedKeys, string(k))

		}
		elapsed := time.Since(iterStart)
		log.Printf("iterate keys with %d items took %s", nbEvents, elapsed)
		return nil
	}); err != nil {
		panic(err)
	}

	// validate badger iteration order is equal to original creation order
	var retrieved []FakeEvent
	if err := db.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		iterStart := time.Now()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			// k := item.Key()
			v, err := item.Value()
			if err != nil {
				panic(err)
			}

			// deserialize the fake events and store them
			var des FakeEvent
			err = json.Unmarshal(v, &des)
			if err != nil {
				panic(err)
			}

			retrieved = append(retrieved, des)

		}
		elapsed := time.Since(iterStart)
		log.Printf("iterate with %d items took %s", nbEvents, elapsed)

		return nil
	}); err != nil {
		panic(err)
	}

}
