package evs

import (
	"math/rand"
	"time"

	"github.com/oklog/ulid"
)

// GetMonotonicEntropy : get a monotonic entropy for ulid
//     usage :
//         entropy := GetMonotonicEntropy(time.Now())
//		   u := ulid.MustNew(ulid.Timestamp(x), entropy)
//
func GetMonotonicEntropy(t time.Time) *ulid.MonotonicEntropy {
	return ulid.Monotonic(rand.New(rand.NewSource(t.UnixNano())), 0)
}

// UlidTimeBoundaries : return ulid  pair for start and stop
//                      given a starting timestamp and a duration
//
func UlidTimeBoundaries(start time.Time, duration time.Duration) (u0, u1 ulid.ULID, err error) {

	// compute end time  start + duration + 1 ms ( because ulid )
	end := start.Add(duration).Add(1 * time.Millisecond)

	u0, err = ulid.New(ulid.Timestamp(start), nil)
	if err != nil {
		return u0, u1, err
	}
	u1, err = ulid.New(ulid.Timestamp(end), nil)
	if err != nil {
		return u0, u1, err
	}
	//fmt.Println(u0.String() + " " + u1.String() + "\n")
	return u0, u1, err
}

// TimestampFromUlid : return timestamp corresponding to a ulid string ( eg 0000XSNQVWCB2JB92NG4NB2G5C )
func TimestampFromUlid(u string) (ts time.Time, err error) {

	id, err := ulid.Parse(u)
	if err != nil {
		return ts, err
	}
	// println(id.String())

	// get time in millisecond
	t := id.Time()
	// convert to a time object
	ts = ulid.Time(t)

	return ts, err
}
