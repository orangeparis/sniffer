package evs

/*
	testing code evs (ulid ) : store timestamp with lexicographically sortable keys

	see: https://barkeywolf.consulting/posts/badger-event-store/
*/

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	//"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/oklog/ulid"
)

func TestUlibLib(t *testing.T) {

	x := time.Unix(1000000, 0)
	entropy := ulid.Monotonic(rand.New(rand.NewSource(x.UnixNano())), 0)
	u := ulid.MustNew(ulid.Timestamp(x), entropy)
	// Output: 0000XSNJG0MQJHBF4QX1EFD6Y3
	fmt.Println(u.String())
	if u.String() != "0000XSNJG0MQJHBF4QX1EFD6Y3" {
		t.Fail()
		return
	}

	// create ulid with no entropy
	t2, err := ulid.New(ulid.Timestamp(x), nil)
	if err != nil {
		t.Fail()
		return
	}
	fmt.Println(t2.String())

}

func TestUlidTimeBoundaries(t *testing.T) {

	x := time.Unix(1000000, 0)
	t0, t1, err := UlidTimeBoundaries(x, 2*time.Second)
	fmt.Printf("%s,%s\n", t0, t1)

	if t0.String() != "0000XSNJG00000000000000000" || t1.String() != "0000XSNMEG0000000000000000" || err != nil {
		t.Fail()
		return
	}

}

func TestTimeStampFromUlid(t *testing.T) {

	// an arbitrary linux timestamp
	t0 := time.Unix(1000000, 0)
	ts0 := t0.String() //    "1970-01-12 14:46:40 +0100 CET"
	println(ts0)

	// prepare an ulid for this timestamp
	entropy := GetMonotonicEntropy(t0)

	// compute the ulid for t0
	id, err := ulid.New(ulid.Timestamp(t0), entropy)
	if err != nil {
		t.Fail()
		return
	}
	u := id.String() //    0000XSNJG0MQJHBF4QX1EFD6Y3
	println(u)

	// compute back the time stamp from this ulid
	tx, err := TimestampFromUlid(u)
	tsx := tx.String()

	// check those two time are equal
	if tsx != ts0 {
		t.Fail()
	}

	fmt.Println(tsx)

}
