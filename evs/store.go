package evs

import (
	"context"
	"log"
	"time"

	//"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/dgraph-io/badger"
	"github.com/oklog/ulid"
)

/*
	store

	store for chronological event store (based on badger.uio and uild )


	WindowKeys( db , start , duration ) keys []string

*/

// StoredEvent : an opaque event to store
// type StoredEvent struct {
// 	Timestamp time.Time
// 	Data      []byte // opaque data , usually a json representation of an event
// }

// Store :the store is based on badger db
// adding entropy for ulid computation
type Store struct {
	*badger.DB
	Name    string                 // an arbitrary name
	entropy *ulid.MonotonicEntropy // entropy for ulid
}

// Init store ( compute a bas entropy )
func (s *Store) Init() {

	ts0 := time.Unix(1000000, 0)

	// reproducible entropy source
	s.entropy = GetMonotonicEntropy(ts0)

}

// StoreEvent : store the event with ulid key derived from timestamp ( keep chronological order )
func (s *Store) StoreEvent(events ...Event) (err error) {

	// initialize entropy if needed
	if s.entropy == nil {
		s.Init()
	}

	// open a database transaction
	txn := s.NewTransaction(true)

	for _, e := range events {

		// if e.Timestamp == nil {
		// 	e.Timestamp = time.Now()
		// }

		// create ulid with entropy
		ts, _ := e.GetTimestamp()
		data := e.Data

		id, err := ulid.New(ulid.Timestamp(ts), s.entropy)
		if err != nil {
			return err
		}
		// serialize the ULID to its binary form
		binID, err := id.MarshalBinary()
		if err != nil {
			return (err)
		}

		// add the insert operation to the transaction
		// and open a new transaction if this one is full
		//e := badger.NewEntry(Key:binID,Value:data)
		//e := s.NewEntry(binID, data)
		e := badger.NewEntry(binID, data)
		if err := txn.SetEntry(e); err == badger.ErrTxnTooBig {
			//if err := txn.Set(binID, data); err == badger.ErrTxnTooBig {
			if err := txn.Commit(); err != nil {
				return err
			}
			txn = s.NewTransaction(true)
			if err := txn.Set(binID, data); err != nil {
				return err
			}
		}
	}
	// flush the transaction
	if err := txn.Commit(); err != nil {
		return err
	}

	return err
}

// WindowKeys : return a list of ordered keys within the time window
func (s *Store) WindowKeys(start time.Time, duration time.Duration) (keys []string, err error) {

	b0, b1, err := UlidTimeBoundaries(start, duration)
	if err != nil {
		return keys, err
	}
	log.Printf("WindowKeys: boundary keys : [%s -> %s]\n", b0, b1)
	// create badger view to collect keys
	if err := s.View(func(txn *badger.Txn) error {

		// create a Badger iterator with the default settings
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()

		// have the iterator walk the LMB tree
		iterStart := time.Now()

		// read loop
		for it.Seek(b0.Bytes()); it.Valid(); it.Next() {
			item := it.Item()

			// rebuild ulid from key bytes
			k := item.Key()
			x := ulid.ULID{}
			x.UnmarshalBinary(k)
			current := x.String()

			//
			// test end of time window
			//
			if string(current) > b1.String() {
				// outside of window
				log.Printf("last key: n=%s\n", current)
				break
			} else {
				// within the window: store it
				keys = append(keys, current)
			}
		}
		elapsed := time.Since(iterStart)
		log.Printf("collecting %d items took %s", len(keys), elapsed)

		return nil
	}); err != nil {
		return keys, err
	}

	return keys, err
}

// WindowRead :
//     b0 : ulid  string for start   eg 0000XSNQC8D56SJRAVX4EDS9NG -> 1970-01-12 14:46:45 +0100 CET
//     b1 : ulid string for stop     eg 0000XSNQVVJDBVN7Q133QQQ07F -> 1970-01-12 14:46:45.499 +0100 CET
//
func (s *Store) WindowRead(ctx context.Context, b0 string, b1 string) (event chan Event, err error) {

	log.Printf("WindowKeys: boundary keys : [%s -> %s]\n", b0, b1)

	// create channel
	event = make(chan Event)

	go func() {
		// create badger view to collect keys
		err = s.View(func(txn *badger.Txn) error {

			// create a Badger iterator with the default settings
			opts := badger.DefaultIteratorOptions
			opts.PrefetchSize = 10
			it := txn.NewIterator(opts)
			defer it.Close()

			// have the iterator walk the LMB tree
			//iterStart := time.Now()

			// seek for first item
			start, err := ulid.Parse(b0)
			if err != nil {
				e := NewErrorEvent("400", "Bad ULID identifer b0")
				event <- e
				return nil
			}
			it.Seek([]byte(start.Bytes()))

			// read loop
			for ; it.Valid(); it.Next() {
				item := it.Item()

				// build an ulid from key
				k := item.Key()
				x := ulid.ULID{}
				x.UnmarshalBinary(k)
				current := x.String()

				//
				// test end of time window
				//
				if string(current) > b1 {
					// outside of window : push null event to indicate we have all data
					e := NewErrorEvent("200", "OK")
					log.Printf("last key: n=%s\n", current)
					event <- e
					return nil
				}

				// within the window: push to channel
				e := Event{}
				v := make([]byte, item.ValueSize())
				value, err := item.ValueCopy(v)
				if err != nil {
					// cannot extract value return an error event
					e = NewErrorEvent("400", "cannot extract value")
				} else {
					// ok fill the event
					e.Data = value
					e.ID = current

				}
				//t, _ = TimestampFromUlid(current)
				//e := &StoredEvent{Timestamp: t, Data: v}
				event <- e

			}
			//elapsed := time.Since(iterStart)
			//log.Printf("collecting %d items took %s", len(keys), elapsed)
			// close channel
			e := NewErrorEvent("404", "Enf Of File")
			event <- e
			//close(event)
			return nil
		})
		if err != nil {
			e := NewErrorEvent("500", "Failed to create view: "+err.Error())
			event <- e
			// close channel
			// close(event)
		}
	}()

	return event, err
}

// Fetch load events form keys ( ulid keys  like "0000XSNQVWCB2JB92NG4NB2G5C" )
func (s *Store) Fetch(keys ...string) (events []Event, err error) {

	return events, err
}

//
func (s *Store) ExperimentalRead(ctx context.Context, start time.Time, duration time.Duration) (event chan Event, err error) {

	// create channel
	event = make(chan Event)

	// compute boundaries
	b0, b1, err := UlidTimeBoundaries(start, duration)
	if err != nil {
		return nil, err
	}
	log.Printf("WindowKeys: boundary keys : [%s -> %s]\n", b0, b1)

	go func() {

		// set timeout with a margin
		timer := time.NewTimer(duration + 5*time.Second)
		defer timer.Stop()

		starter := b0

		// create badger view to collect events
		err = s.View(func(txn *badger.Txn) error {

			// create a Badger iterator with the default settings
			opts := badger.DefaultIteratorOptions
			opts.PrefetchSize = 10
			it := txn.NewIterator(opts)
			defer it.Close()

			current := ""

			// main loop
			for {

				// seek b0 mark
				it.Seek(starter.Bytes())

				// read loop
				for ; it.Valid(); it.Next() {
					item := it.Item()

					// rebuild ulid from key bytes
					k := item.Key()
					x := ulid.ULID{}
					x.UnmarshalBinary(k)
					current = x.String()

					//
					// test end of time window
					//
					if string(current) > b1.String() {
						// outside of window : push null event to indicate we have all data
						e := NewErrorEvent("200", "OK")
						log.Printf("last key: n=%s\n", current)
						event <- e
						return nil
					}

					// within the window: push to channel
					e := Event{}
					v := make([]byte, item.ValueSize())
					value, err := item.ValueCopy(v)
					if err != nil {
						// cannot extract value return an error event
						event <- NewErrorEvent("400", "cannot extract value")
						return nil
					}

					// ok fill the event and send
					e.Data = value
					e.ID = current
					event <- e
				} // end of read loop

				// cross point
				// watch for
				//      ctx.Done() has parent asked for stop
				//      it.next()  is there another record to read
				//      timer

				// check end condition
				select {
				case <-ctx.Done():
					log.Println("Done.")
					return nil
				case <-timer.C:
					log.Println("Timed out")
					event <- NewErrorEvent("404", "timeout")
					return nil
				default:
					// wait before launching a new read loop
					time.Sleep(1 * time.Second)
				}

				// prepare a new read loop
				log.Println("Next read loop")
				if current != "" {
					// set a new startr
					starter, err = ulid.Parse(current)
					if err != nil {
						event <- NewErrorEvent("400", "cannot convert curren to ulid")
						return nil
					}
				}

			} // end of main
		})
		if err != nil {
			e := NewErrorEvent("500", "Failed to create view: "+err.Error())
			event <- e
			// close channel
			// close(event)
		}

	}()

	return event, err
}
