package evs_test

/*
	testing ulid tools : store timestamp with lexicographically sortable keys


*/

import (
	"context"
	"encoding/json"
	"log"
	"testing"
	"time"

	"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/dgraph-io/badger"
	"github.com/oklog/ulid"
)

type SampleEvent struct {
	Number    int // we also add a number to keep track of the ground-truth creation order
	Timestamp time.Time
	ID        ulid.ULID
}

// create a slice of events (timestamps)
func sample_events(initial time.Time, n int, delta time.Duration) (events []evs.Event) {

	ts := initial
	for i := 0; i < n; i++ {

		// create an event with no entropy ( timestamp ) and a blank data field
		evts, err := evs.NewTimestamp([]byte{}, ts)
		if err != nil {
			log.Fatal("cannot compute ulid from timestamp")
			return
		}
		ulid, _ := evts.GetUlid()
		payload := SampleEvent{i, ts, ulid}

		// serialize the event payload (to JSON for simplicity)
		data, err := json.Marshal(payload)
		if err != nil {
			log.Fatal("cannot serialize to json")
		}
		evts.Data = data

		//ev, _ := evs.NewTimestamp(data, ts)
		//ev := evs.StoredEvent{Timestamp: ts, Data: data}

		events = append(events, evts)

		// increment ts
		ts = ts.Add(delta)

	}
	firstEvent, _ := events[0].GetTimestamp()
	lastEvent, _ := events[len(events)-1].GetTimestamp()
	timeRange := lastEvent.Sub(firstEvent)
	log.Printf(" time range of %d items is %s", n, timeRange)
	return events
}

func TestStore(t *testing.T) {

	// create a store
	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)

	store := &evs.Store{DB: db}
	defer store.Close()

	// create a set of events
	var nbEvents = 100000
	ts0 := time.Unix(1000000, 0)
	delta, _ := time.ParseDuration("100us")
	events := sample_events(ts0, nbEvents, delta)
	log.Printf("create %d events, starting at %s\n", nbEvents, ts0)

	// write events to db
	err := store.StoreEvent(events...)
	if err != nil {
		log.Printf("cannot create events in database")
		t.Fail()
	}

	// queries

	var p1, p2 string
	var t1, t2 time.Time

	//
	// select keys from a time range within window
	//
	start := ts0.Add(5 * time.Second)
	delay, _ := time.ParseDuration("500ms")

	keys, err := store.WindowKeys(start, delay)
	log.Printf("keys collected within window: %d\n", len(keys))

	if len(keys) > 0 {

		p1 = keys[0]
		t1, _ = evs.TimestampFromUlid(p1)
		log.Printf("first ulid key : t= %s -> %s\n", p1, t1)

		p2 = keys[len(keys)-1]
		t2, _ = evs.TimestampFromUlid(p2)
		log.Printf("last  ulid key : t= %s -> %s\n", p2, t2)

	}

	// select keys from a time range not in the window ( later)
	start = ts0.Add(20 * time.Second)
	delay, _ = time.ParseDuration("500ms")

	keys, err = store.WindowKeys(start, delay)
	log.Printf("keys collected after window: %d\n", len(keys))

	if len(keys) > 0 {
		p1 = keys[0]
		t1, _ = evs.TimestampFromUlid(p1)
		log.Printf("first ulid key : t= %s -> %s\n", p1, t1)

		p2 = keys[len(keys)-1]
		t2, _ = evs.TimestampFromUlid(p2)
		log.Printf("last  ulid key : t= %s -> %s\n", p2, t2)
	}

	// select keys from a time range not in the window ( before )
	start = ts0.Add(-10 * time.Second)
	delay, _ = time.ParseDuration("500ms")

	keys, err = store.WindowKeys(start, delay)
	log.Printf("keys collected before window: %d\n", len(keys))

	if len(keys) > 0 {
		p1 = keys[0]
		t1, _ = evs.TimestampFromUlid(p1)
		log.Printf("first ulid key : t= %s -> %s\n", p1, t1)

		p2 = keys[len(keys)-1]
		t2, _ = evs.TimestampFromUlid(p2)
		log.Printf("last  ulid key : t= %s -> %s\n", p2, t2)
	}

	println("Done")

}

func TestRead(t *testing.T) {

	// create a store
	tmpDir := "./badgerdb_tmp"
	opts := badger.DefaultOptions(tmpDir)
	//opts.Dir = tmpDir
	//opts.ValueDir = tmpDir
	db, _ := badger.Open(opts)

	store := &evs.Store{DB: db}
	defer store.Close()

	// create a set of events
	var nbEvents = 100000
	ts0 := time.Unix(1000000, 0)
	delta, _ := time.ParseDuration("100us")
	events := sample_events(ts0, nbEvents, delta)
	log.Printf("create %d events, starting at %s\n", nbEvents, ts0)

	// write events to db
	err := store.StoreEvent(events...)
	if err != nil {
		log.Printf("cannot create events in database")
		t.Fail()
	}

	// queries

	var e1, e2 evs.Event
	var t1, t2 time.Time

	//
	// select keys from a time range within window
	//
	start := ts0.Add(5 * time.Second)
	delay, _ := time.ParseDuration("500ms")

	b0, b1, err := evs.UlidTimeBoundaries(start, delay)
	if err != nil {
		t.Fail()
		return
	}

	// try to read keys if any
	keys, err := store.WindowKeys(start, delay)
	log.Printf("keys collected within window: %d\n", len(keys))

	ctx := context.Background()
	eventChan, err := store.WindowRead(ctx, b0.String(), b1.String())

	if err != nil {
		log.Printf("WindowRead failed:%s", err.Error())
		t.Fail()
		return
	}

	var collected []evs.Event
	var info map[string]interface{}

	for ev := range eventChan {
		valid, reason := ev.IsValid()
		if valid {
			collected = append(collected, ev)
			info := json.Unmarshal(ev.Data, &info)
			println("event: ", ev.ID, info)
		} else {
			println("error event :", reason)
			break
		}
	}

	if len(collected) > 0 {
		println("collected : ", len(collected))
		e1 = collected[0]
		t1, _ = e1.GetTimestamp()
		log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)

		e2 = collected[len(collected)-1]
		t2, _ = e2.GetTimestamp()
		log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

	} else {
		println("no event collected")
	}

	//
	// select keys from a time range  past the storage
	//
	collected = []evs.Event{}
	start = ts0.Add(9 * time.Second).Add(900 * time.Millisecond)
	delay, _ = time.ParseDuration("2s")

	b0, b1, err = evs.UlidTimeBoundaries(start, delay)
	if err != nil {
		t.Fail()
		return
	}
	eventChan, err = store.WindowRead(ctx, b0.String(), b1.String())

	if err != nil {
		log.Printf("WindowRead failed:%s", err.Error())
		t.Fail()
		return
	}

	for ev := range eventChan {
		valid, reason := ev.IsValid()
		if valid {
			collected = append(collected, ev)
			//println("event: ", ev.ID)
		} else {
			println("error event :", reason)
			break
		}
	}

	if len(collected) > 0 {
		println("collected : ", len(collected))
		e1 = collected[0]
		t1, _ = e1.GetTimestamp()
		log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)

		e2 = collected[len(collected)-1]
		t2, _ = e2.GetTimestamp()
		log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

	} else {
		println("no event collected")
	}

	println("Done")

}

// func TestWait(t *testing.T) {

// 	// create a store
// 	tmpDir := "./badgerdb_tmp"
// 	opts := badger.DefaultOptions(tmpDir)
// 	//opts.Dir = tmpDir
// 	//opts.ValueDir = tmpDir
// 	db, _ := badger.Open(opts)

// 	store := &evs.Store{DB: db}
// 	defer store.Close()

// 	// create a set of events
// 	var nbEvents = 100000
// 	ts0 := time.Unix(1000000, 0)
// 	delta, _ := time.ParseDuration("100us")
// 	events := sample_events(ts0, nbEvents, delta)
// 	log.Printf("create %d events, starting at %s\n", nbEvents, ts0)

// 	// write events to db
// 	err := store.StoreEvent(events...)
// 	if err != nil {
// 		log.Printf("cannot create events in database")
// 		t.Fail()
// 	}

// 	// queries

// 	var e1, e2 evs.Event
// 	var t1, t2 time.Time

// 	//
// 	// select keys from a time range within window
// 	//
// 	start := ts0.Add(5 * time.Second)
// 	delay, _ := time.ParseDuration("500ms")

// 	b0, b1, err := evs.UlidTimeBoundaries(start, delay)
// 	if err != nil {
// 		t.Fail()
// 		return
// 	}

// 	ctx := context.Background()
// 	eventChan, err := store.WindowRead(ctx, b0.String(), b1.String())

// 	if err != nil {
// 		log.Printf("WindowRead failed:%s", err.Error())
// 		t.Fail()
// 		return
// 	}

// 	var collected []evs.Event

// 	for ev := range eventChan {
// 		valid, reason := ev.IsValid()
// 		if valid {
// 			collected = append(collected, ev)
// 			//println("event: ", ev.ID)
// 		} else {
// 			println("error event :", reason)
// 			break
// 		}
// 	}

// 	if len(collected) > 0 {
// 		println("collected : ", len(collected))
// 		e1 = collected[0]
// 		t1, _ = e1.GetTimestamp()
// 		log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)

// 		e2 = collected[len(collected)-1]
// 		t2, _ = e2.GetTimestamp()
// 		log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

// 	} else {
// 		println("no event collected")
// 	}

// 	// //
// 	// // select keys from a time range  past the storage
// 	// //
// 	// collected = []evs.Event{}
// 	// start = ts0.Add(9 * time.Second).Add(900 * time.Millisecond)
// 	// delay, _ = time.ParseDuration("2s")

// 	// eventChan, err = store.WindowRead(ctx, start, delay)

// 	// if err != nil {
// 	// 	log.Printf("WindowRead failed:%s", err.Error())
// 	// 	t.Fail()
// 	// 	return
// 	// }

// 	// for ev := range eventChan {
// 	// 	valid, reason := ev.IsValid()
// 	// 	if valid {
// 	// 		collected = append(collected, ev)
// 	// 		//println("event: ", ev.ID)
// 	// 	} else {
// 	// 		println("error event :", reason)
// 	// 		break
// 	// 	}
// 	// }

// 	// if len(collected) > 0 {
// 	// 	println("collected : ", len(collected))
// 	// 	e1 = collected[0]
// 	// 	t1, _ = e1.GetTimestamp()
// 	// 	log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)

// 	// 	e2 = collected[len(collected)-1]
// 	// 	t2, _ = e2.GetTimestamp()
// 	// 	log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

// 	// } else {
// 	// 	println("no event collected")
// 	// }

// 	println("Done")

// }
