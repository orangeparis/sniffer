# sniffer

a go executable to watch a network interface and publish nats messages on topic sniffer.>

## usage

## read from network interface

    sniffer --name 1 --interface eth0 --nats nats://192.168.99.100:4222 

read packets from network interface eth0
and publish message to nats server at nats://192.168.99.100:4222
with subjects like  sniffer.1.>

## read from pcap file

    sniffer --name 2--pcap="./sample.pcap --nats: nats://demo.nats.io:4222 

read packets from pcap file sample.pcap 
and publish message to nats server nats://demo.nats.io:4222
with subjects like sniffer.2.>

## read from pcap file , publish to screen for testing

    sniffer --name 2 --pcap="./sample.pcap --screen

## read from pcap fil , publish to screen for testing

    sniffer --name 1 --interface eth0 --screen 
