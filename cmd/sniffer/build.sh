# clean up
rm  ./sniffer-.*



echo "compile for darwin"
#env GOOS=darwin GOARCH=amd64 go build -o sniffer-darwin-amd64 .

env GOOS=darwin GOARCH=amd64 go build -o ../../build/packages/darwin_amd64/sniffer .


echo "warning cross compilation does not work for linux cause we need libcap"
## env GOOS=linux GOARCH=amd64  CGO_ENABLED=1 go build -o sniffer-linux-amd64 .

# to compile sniffer for linux we need a docker image or a vagrant vm