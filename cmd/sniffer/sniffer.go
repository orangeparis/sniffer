package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/orangeparis/ines/heartbeat"
	"bitbucket.org/orangeparis/ines/nping"
	"bitbucket.org/orangeparis/sniffer"
	"bitbucket.org/orangeparis/sniffer/devicePlayer"
	"bitbucket.org/orangeparis/sniffer/filePlayer"
	"bitbucket.org/orangeparis/sniffer/natspublisher"
	"bitbucket.org/orangeparis/sniffer/screenpublisher"
)

var (
	Version = "0.1"

	name = flag.String("name", "X", "name of the sniffer (eg 0 , will publish to sniffer.0.>)")

	networkInterface = flag.String("interface", "eth0", "network interface (eg eth0)")
	pcapFile         = flag.String("pcap", "", "pcap file to read (eg: sample.pcap)")

	nats   = flag.String("nats", "nats://127.0.0.1:4222", "nats server (eg nats://demo.nats.io:4222)")
	screen = flag.Bool("screen", false, "publish to screen")

	bpf = flag.String("bpf", "", "bpf filter (eg 'portrange 67-68'")

	helper = `Scan a network interface OR a pcap file and send message to nats server OR screen
sample usage : sniffer --name s1 --interface eth0 --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "X") (eg 0 , will publish to sniffer.0.>) 

--interface string
    	network interface (default "eth0")
--pcap string
		pcap file to read (eg: sample.pcap)
		
--nats string
    	nats server (default "nats://demo.nats.io:4222")
--screen
    	publish to screen
--bpf string
	set a bpf filter eg "portrange 67-68"" or "all" if you dont want to apply any filter  
`
)

func getPublisher() (publisher sniffer.Publisher, err error) {

	// return a publisher
	//  if --screen return a screen publisher
	//  else return a nats publisher

	// topic   eg sniffer.0.  sniffer.labo2.
	var topic string

	//if strings.HasSuffix(*name, ".") {
	topic = fmt.Sprintf("sniffer.%s", *name)
	//} else {
	//	topic = fmt.Sprintf("sniffer.%s.", *name)
	//}

	if *screen == true {
		publisher = screenpublisher.NewScreenPublisher(topic)
		log.Println("sniffer : set Publisher to screen")

	} else {
		// a nats publisher
		natsURL := *nats
		publisher, err = natspublisher.NewNatsPublisher(topic, natsURL)
		log.Printf("sniffer : set Publisher to nats publisher at %s", natsURL)

		// add a heartbeat emitter Heartbeat.sniffer.1
		full_name := "sniffer." + *name
		e, _ := heartbeat.NewEmitter(full_name, natsURL, 10)
		e.Start()
		log.Printf("sniffer : started a heartbeat emitter [Heartbeat.%s] at  %s", full_name, natsURL)

		// handle call.
		c, err := nping.NewClient(*nats)
		if err != nil {
			log.Printf("Warning: failed to start handler for call.%s.ping\n", topic)
			return publisher, err
		}
		defer c.Close()
		if err == nil {
			//pingTopic := "call." + topic + ".ping"
			sub, err := c.HandlePingService(topic)
			if err != nil {
				log.Printf("Warning: failed to subscribe to ping topic: %s\n", topic)
				return publisher, err
			} else {
				defer sub.Unsubscribe()
				pingTopic := "call." + topic + ".ping"
				log.Printf("start handler for ping service at: %s\n", pingTopic)

			}

		}

	}
	log.Printf("sniffer : set root topic to: %s\n", topic)

	return publisher, err
}

func pcapMode(publisher sniffer.Publisher, pcap string, bpfFilter string) {

	//defer publisher.Close()

	player, err := filePlayer.NewPlayer(pcap, publisher, bpfFilter)
	if err != nil {
		log.Printf("sniffer : error %s\n", err.Error())
		log.Fatal("sniffer : aborted.")
	}

	// launch the player loop
	log.Println("sniffer : start player")
	player.Start()

	// flush publisher before quiting ??
	player.Close()

	//
	time.Sleep(10 * time.Second)

}

func interfaceMode(publisher sniffer.Publisher, device string, bpfFilter string) {

	player, err := devicePlayer.NewDevicePlayer(device, publisher, bpfFilter)
	if err != nil {
		log.Printf("sniffer : error: %s\n", err.Error())
		log.Fatal("sniffer : aborted.")
	}

	// launch the player loop
	log.Println("sniffer : start player")
	player.Start()

}

func debugflag() {

}

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("sniffer %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// get a publisher corresponding to the flags ( screen or nats )
	publisher, err := getPublisher()
	if err != nil {
		log.Fatalf("sniffer: cannot get a publisher: %s\n", err.Error())
		os.Exit(1)
	}

	// set bpf filter
	bpfFilter := *bpf

	// find  mode ( interface or pcap )
	mode := "INTERFACE"
	if *pcapFile != "" {
		mode = "PCAP"
	}

	//log.Printf("sniffer: starting")
	switch mode {
	case "PCAP":
		// read from a pcap file
		log.Printf("sniffer: create a pcap reader with %s\n", *pcapFile)
		pcapMode(publisher, *pcapFile, bpfFilter)

	default:
		// read from network interface (need to be root )
		log.Printf("sniffer : create a device reader with %s\n", *networkInterface)
		interfaceMode(publisher, *networkInterface, bpfFilter)
	}
	log.Printf("sniffer : exiting")

}

func main() {
	run()
}
