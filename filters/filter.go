package filters

import (
	"encoding/json"
	"fmt"
	"log"

	//"time"

	"bitbucket.org/orangeparis/sniffer"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	//"strconv"
)

/*
	filter scan packets to detect specific traffic

	ptrocol and DstPort

	* dhcp      ( full support )
	* radius  1812,1813   ( minimal support )
	* ldp : 646   Label distribution protocol  ( no support)
	* snmp : 161 Simple network management protocol (no support )
	* multicast dns : 5353 , ( no support )


	Well Known Ports: 0 through 1023.
	Registered Ports: 1024 through 49151.
	Dynamic/Private : 49152 through 65535.

*/

type FilterEngine interface {
	Filter(rawPacket *gopacket.Packet) (event map[string]interface{}, err error)
}

type Filter struct {
	publisher           sniffer.Publisher
	packetCounter       uint64
	DhcpMessageSequence int // keep track of sniffer.$name...dhcp message sent
}

// PublishDhcpMessage : publish dhcp message with sequence
func (f *Filter) PublishDhcpMessage(topic string, msg []byte) {
	subject := fmt.Sprintf("%s.%d", topic, f.DhcpMessageSequence)
	err := f.publisher.Publish(subject, msg)
	if err != nil {
		return
	}
	// increment sequence
	f.DhcpMessageSequence += 1
}

var (
	macAddrFilter string = "02:ff:ff:e0:88:41"
	vlanFilter    int    = 2900
)

func NewFilter(publisher sniffer.Publisher) (filter *Filter) {

	log.Printf("create new Filter\n")

	filter = &Filter{
		publisher: publisher,
	}

	return filter
}

// Filter : filters a packet : publish message and return an event map
func (f *Filter) Filter(packet gopacket.Packet, packetId uint32) (event map[string]interface{}, err error) {
	//
	//
	//
	event = make(map[string]interface{})
	event["_verdict"] = "PASS"
	event["udpLen"] = 0

	var (
		eth layers.Ethernet
		ip4 layers.IPv4
		ip6 layers.IPv6
		tcp layers.TCP
		udp layers.UDP
		dns layers.DNS
		//sip  layers.SIP
		body gopacket.Payload
	)

	var vlan layers.Dot1Q
	var dhcp layers.DHCPv4
	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&dhcp,
		&vlan,
		&eth,
		&ip4,
		&ip6,
		&tcp,
		&udp,
		&dns,
		//&sip,
		&body)

	var foundLayerTypes []gopacket.LayerType
	err = parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		//log.Printf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		return event, err
	}

	isDns := false
	isDhcp := false
	isRadius := false

	var vlanId int

	for _, layer := range foundLayerTypes {
		switch layer {
		case layers.LayerTypeDHCPv4:
			isDhcp = true
		case layers.LayerTypeDot1Q:
			event["lanId"] = vlan.VLANIdentifier
			event["type"] = vlan.Type
			vlanId = int(vlan.VLANIdentifier)
		case layers.LayerTypeEthernet:
			event["SrcMAC"] = eth.SrcMAC.String()
			event["DstMAC"] = eth.DstMAC.String()
		case layers.LayerTypeIPv4:
			event["SrcIP"] = ip4.SrcIP.String()
			event["DstIP"] = ip4.DstIP.String()
			event["ip4Len"] = ip4.Length
		case layers.LayerTypeIPv6:
			event["SrcIP"] = ip6.SrcIP.String()
			event["DstIP"] = ip6.DstIP.String()
		case layers.LayerTypeUDP:
			event["udpLen"] = udp.Length
			event["SrcPort"] = udp.SrcPort
			event["DstPort"] = udp.DstPort
		case layers.LayerTypeDNS:
			isDns = true
		}
	}

	msg := "other"

	// handle packet counter ( send a message every 100 packets)
	f.packetCounter++
	if f.packetCounter%100 == 0 {
		// every 100 packets received we send a message
		topic := fmt.Sprintf("stats.packet.counter")
		message := fmt.Sprintf(`{"counter":%d}`, f.packetCounter)
		f.publisher.Publish(topic, []byte(message))
	}

	_ = isDns
	_ = vlanId

	// handle dhcp  root.02:ff:ff:e0:88:41.dhcp.*
	if isDhcp {

		var topic string

		// provision event with dhcp layers info
		HandleDhcp(event, dhcp)
		op := dhcp.Operation.String()
		dhcpType, ok := event["DHCPMsgType"]
		if !ok {
			// dont have a msgtype (Discover/Offer/Request/Ack get the operation instead (Request/Reply)
			dhcpType = op
		}

		// dhcp.ClientHWAddr
		if dhcp.ClientHWAddr.String() != "" {
			// we got a dhcp client address => make the topic with it
			topic = fmt.Sprintf("%s.dhcp.%s", dhcp.ClientHWAddr.String(), dhcpType)
		} else {
			// we dont have a dhcp client

			// by default publish on srcMac channel: <SrcMac>.dhcp.<op>
			topic = fmt.Sprintf("%s.dhcp.%s", eth.SrcMAC.String(), op)
			switch op {
			case "Request":
				// assume it is a livebox request send a dhcp message   root.<srcMac>.dhcp.<op>
				//topic = fmt.Sprintf("%s.dhcp.%s", eth.SrcMAC.String(), op)
			case "Reply":
				// assume it is a dhcp reply to the livebox send a dhcp message root.<DstMac>.dhcp.<op>
				topic = fmt.Sprintf("%s.dhcp.Reply", eth.DstMAC.String())
			default:
				// unkwonw dhcp type ???? should not happen
				log.Printf("dhcp filter: unknown type: %s", op)
			}
		}

		// Marshal the map into a JSON string.
		empData, err := json.Marshal(event)
		if err != nil {
			log.Println(err.Error())
			return event, err
		}

		msg = string(empData)
		// fmt.Println("The JSON data is:")
		// fmt.Println(msg)

		//f.publisher.Publish(topic, []byte(msg))
		f.PublishDhcpMessage(topic, []byte(msg))
		return event, err
	}

	// if udp.DstPort != 0 && udp.DstPort != 53 {
	// 	log.Printf("catch a DstPort: %d\n", udp.DstPort)
	// }

	// radius detection
	if true {
		if udp.SrcPort == 1812 || udp.SrcPort == 1813 || udp.DstPort == 1812 || udp.DstPort == 1813 {
			isRadius = true
			_ = isRadius
			topic := fmt.Sprintf("%s.radius.%s", eth.SrcMAC.String(), "generic")
			// Marshal the map into a JSON string.
			empData, err := json.Marshal(event)
			if err != nil {
				log.Println(err.Error())
				return event, err
			}

			msg = string(empData)
			// fmt.Println("The JSON data is:")
			// fmt.Println(msg)

			f.publisher.Publish(topic, []byte(msg))
			return event, err
		}
	}

	return event, err
}
