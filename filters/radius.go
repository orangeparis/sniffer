package filters

import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/orangeparis/sniffer"
)

// // MaxPacketLength is the maximum wire length of a RADIUS packet.
// const MaxPacketLength = 4095

// // Packet is a RADIUS packet.
// type Packet struct {
// 	Code          Code
// 	Identifier    byte
// 	Authenticator [16]byte
// 	Secret        []byte
// 	Attributes
// }

/*
project to handle radius packets

handling radius packets


# protocole :

see https://blog.pingex.net/comprendre-radius-part1-protocole/

UDP / 1813  ( 1812 )

# verbs:

	ACCESS-REQUEST
	CHALLENGE-REQUEST
	ACCESS-ACCEPT
	ACCESS-REJECT



# attributs:

User-Password: Mot de passe PAP, c’est un attribut assez basique,
Framed-IP-Address: Spécifie une adresse IP à attribuer à l’utilisateur, utilisé dans le cadre de tunnels PPP par exemple,
Tunnel-Type: Donne le type de tunnel utilisé (PPTP, IP-in-IP, GRE, etc.),
Cisco-Call-Type:Un attribut utilisé par le matériel Cisco,
3GPP-IMSI: un numéro unique identifiant un usager sur les réseaux mobiles 2/3/4G (attribut du consortium 3GPP).

*/

// HandleRadius : handle radius Messages
func HandleRadius(publisher sniffer.Publisher, event map[string]interface{}, parser Layers) (err error) {

	// Marshal the map into a JSON string.
	data, err := json.Marshal(event)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	op := "generic"
	topic := fmt.Sprintf("%s.radius.%s", parser.Eth.SrcMAC.String(), op)
	//msg := string(empData)
	// fmt.Println("The JSON data is:")
	// fmt.Println(msg)

	publisher.Publish(topic, data)

	return err
}

// // Parse parses an encoded RADIUS packet b. An error is returned if the packet
// // is malformed.
// func Parse(b, secret []byte) (*Packet, error) {
// 	if len(b) < 20 {
// 		return nil, errors.New("radius: packet not at least 20 bytes long")
// 	}

// 	length := int(binary.BigEndian.Uint16(b[2:4]))
// 	if length < 20 || length > MaxPacketLength || len(b) != length {
// 		return nil, errors.New("radius: invalid packet length")
// 	}

// 	attrs, err := ParseAttributes(b[20:])
// 	if err != nil {
// 		return nil, err
// 	}

// 	packet := &Packet{
// 		Code:       Code(b[0]),
// 		Identifier: b[1],
// 		Secret:     secret,
// 		Attributes: attrs,
// 	}
// 	copy(packet.Authenticator[:], b[4:20])
// 	return packet, nil
// }
