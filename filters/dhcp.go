package filters

import (
	"fmt"

	"github.com/google/gopacket/layers"
)

/*
The Livebox MUST provide the DHCP Server with an "Authentication information" parameter in
DHCP option 90 sent in the DHCP DISCOVER/REQUEST Messages.
Replay Detection is not controlled.
Only the simple configuration token mode where Authentication information is unencoded (Protocol
field=0, Algorithm=0 and RDM=0) as defined in [RFC 3118] is supported.
The PPP login (fti/xxxxx) and Password information are sent in "Authentication information"
parameter based on a CHAP [RFC 1994] like method as defined below.
The content of authentication information field is built using a Radius based [RFC 2865] TLV
encoding method specifying:
- Orange Encryption method (T=26, L=9, Vendor-id=0x00000558 T=1, L=3, Value=A)
o 0x00000558 is the hexadecimal value of Orange Vendor Id (1368 registred at
IANA)

- Username (T=1, L= Length of the Username field, Value=unencrypted PPP login
- Challenge (T=60, L=18, Value= Random)
Chap password (T=3, L=19, Value= Chap-Id + encrypted PPP password
DHCP Option 90 content is the same as the DHCPv6 Option11 (see [R10])



*/

// HandleDhcp : handle dchp Messages
func HandleDhcp(event map[string]interface{}, dhcp layers.DHCPv4) (err error) {

	event["dhcpOp"] = dhcp.Operation.String()
	event["ClientHWAddr"] = dhcp.ClientHWAddr.String()
	// event["Xid"] = dhcp.Xid
	//options := dhcp.Options.String()
	//fmt.Printf("options:%s\n", options)
	for _, o := range dhcp.Options {
		if o.Type == layers.DHCPOptMessageType {
			msgType := layers.DHCPMsgType(o.Data[0])
			event["DHCPMsgType"] = msgType.String()
			if event["DHCPMsgType"] == "Ack" {
				fmt.Println("Dhcp Reply Ack")
			}
		}
		if o.Type == 125 {
			// orange option livebox 125  AutoFallBack and ForcedPPP
			data := o.Data
			A := data[7]
			T := data[8]
			P := data[9]
			var CC = [2]byte{data[10], data[11]}
			if A == 1 {
				event["AutoFallBack"] = true
			} else {
				event["AutoFallBack"] = false
			}
			if P == 1 {
				event["ForcedPPP"] = true
			} else {
				event["ForcedPPP"] = false
			}
			event["IPTVRunningInterface"] = fmt.Sprintf("%#v", T)
			_ = T
			_ = CC

		}
		if o.Type == 82 {
			// generic option: DHCP Relay Agent Information Option
			// Ciruit-ID , Remote-ID , Vendor-ID

			relayAgentInformation := "yes"
			_ = relayAgentInformation
			offset := 0
			subOp := int(o.Data[offset])
			offset++
			l := int(o.Data[offset])
			offset++
			if l > 0 {
				data := o.Data[offset : l+offset]
				event["DHCPCircuitID"] = string(data)
				//println(string(data))
				_ = subOp
				_ = data
				// next
				offset += l
				if offset < int(o.Length) {
					subOp2 := int(o.Data[offset])
					offset++
					l2 := int(o.Data[offset])
					offset++
					if l2 > 0 {
						data2 := o.Data[offset : l2+offset]
						event["DHCPRemoteID"] = string(data2)
						//println(string(data2))
						offset += l2
						_ = data2
						_ = subOp2
					}
				}
			}

		}
		if o.Type == 90 {
			// Authentication for DHCP Request Messages (DISCOVER/REQUEST )
			//   code   Length   protocol  algorythm
			//   rdm    -- replay detection -------
			//   ----   -- authentification --------
			//    -----------
			//println("dhcp option 90")

			protocol := o.Data[0]
			algorythm := o.Data[1]
			rdm := o.Data[2]
			replay := o.Data[3:7]
			authent := o.Data[8 : len(o.Data)-10]
			_ = protocol
			_ = algorythm
			_ = rdm
			_ = replay
			_ = authent
			//println("dhcp authent " + string(authent))
			event["DHCPAuthentication"] = string(authent)
			if len(authent) >= 25 {
				event["fti"] = string(authent[14:25])
			}
		}
		if o.Type == 60 {
			// vendor class identifier  eg sagem ...
			//length := o.Data[0]
			//_=length
			event["VendorClassIdentifier"] = string(o.Data)
			//TODO check this

		}
		//fmt.Printf("option: %s\n", o.String())
	}

	return err
}
