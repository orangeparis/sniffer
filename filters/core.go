package filters

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

const (
	PASS = 1
	DROP = 0
)

// Layers : struct grouping data for parser.DecodeLayers
type Layers struct {
	Eth  layers.Ethernet
	Ip4  layers.IPv4
	Ip6  layers.IPv6
	Tcp  layers.TCP
	Udp  layers.UDP
	Dns  layers.DNS
	Body gopacket.Payload
	Vlan layers.Dot1Q
	Dhcp layers.DHCPv4

	VlanId int
}
