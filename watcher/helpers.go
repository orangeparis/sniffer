package watcher

import (
	"log"

	"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/nats-io/nats"
)

// DefaultMessageHandler : a default handler for message
// usage :
// 		topic := prefix + ">"
//		nc.Subscribe(topic, DefaultMessageHandler)
//
func DefaultMessageHandler(m *nats.Msg) {
	log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
}

// DefaultBoundMessageHandler :
//   return a message handler bound to a store
func DefaultBoundMessageHandler(store *evs.Store) func(*nats.Msg) {

	return func(m *nats.Msg) {
		log.Printf("message handler bound to:%s\n", store.Name)
		log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")
	}
}
