watcher



watcher is a http server collecting events from sniffer(s) (nats subscribing) and preparing metrics for prometheus

it publish prometheus data at <host>/metrics

watcher store event in a temporary database to serve queries
watcher provides an api to query events




# events

the watcher subscribe to nats messages on subjects of the form

    <root>.<device>.<event>.<type> 

* root is a prefix eg root,sniffer1,labo ...
* device is the device identifier ( often mac address of the device) 
* event is the general kind of event ( eg dhcp ...)
* type is sub type of event ( eg for event= dhcp , type can be Request or Reply)

the message are json representation they can contains the following fields

* srcMac : the mac address of the device sending the event
* DstMac : the mac address of destination device
* SrcIp : ipv4 address of the sending device
* SrcpPort (integer): the port number 
* ip4len (intger) ipv4 size message
* udplen (integer) len of udp packet
* type  (integer) type of event ( eg 2048 for dhcp )


# metrics

we describe here the metrics to send to prometheus

## labels

device ( the mac address of the device)
ip     ( the ip of the device )


## metrics by name


### received_events  

    a counter for events received by a device
    


### sent_events 
    a counter of the events sent by a device

### dhcp_request
    a counter of dhcp requests

### dncp_reply 
    a counter of dhcp replies






# api 

watcher provides an api for simple event query



eg is device X send a dhcp request within a time delay 

or is device X received a dhcp Reply within a time delay



# event db

an ephemeral local database to keep track of recent events

inspiration : https://barkeywolf.consulting/posts/badger-event-store/