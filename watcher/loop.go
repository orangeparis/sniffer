package watcher

import (
	"context"
	"fmt"
	"log"

	"bitbucket.org/orangeparis/sniffer/evs"
	"github.com/nats-io/go-nats"
)

var (
	natsUrl = "nats://192.168.99.100:4222"
	topic   = ">"
)

// Loop : main loop for watcher
// connect to nats and subscribe to all events
// receive events and store then to event store
func Loop(ctx context.Context, db *evs.Store) (err error) {

	nc, err := nats.Connect(natsUrl)
	if err != nil {
		log.Printf("nats Listener failed :%s", err.Error())
		return err
	}

	defer nc.Close()

	// subscribe to all inputs
	nc.Subscribe(topic, func(m *nats.Msg) {
		//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
		log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

	})

	// wait for cancel
	for {
		log.Printf("nats Listener enter loop (wait for cancel)")
		select {
		case <-ctx.Done(): // Done  this context is canceled -> exit
			fmt.Println("nats listener Exiting")
			return err
		}
	}

}
