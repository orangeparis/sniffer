package watcher_test

import (
	"context"
	"fmt"
	"log"
	"testing"
	"time"

	"bitbucket.org/orangeparis/sniffer/evs"
	"bitbucket.org/orangeparis/sniffer/filePlayer"
	publisher "bitbucket.org/orangeparis/sniffer/natspublisher"
	"github.com/dgraph-io/badger"
	"github.com/nats-io/nats"
)

var (

	//nats_url = "nats://192.168.99.100:4222"
	nats_url         = "nats://demo.nats.io:4222"
	pcapFile  string = "../fixtures/ines_dhcp.pcap"
	prefix    string = "pcap."
	badgerDir string = "./badgerdb_tmp"
	//handle   *pcap.Handle
	err error
)

// TestLoop  test the watcher loop
// launch a demo fileplayer publishing to a nats server
// launch watcher loop
func TestLoop(t *testing.T) {

	// create a nats publisher
	publisher, err := publisher.NewNatsPublisher(prefix, nats_url)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer publisher.Close()

	// create a file player
	player, err := filePlayer.NewPlayer(pcapFile, publisher)
	if err != nil {
		log.Printf("error: %s", err.Error())
		return
	}

	// Launch player
	go player.Start()

	// get a starter mark
	t0 := time.Now()

	// open nats connection
	nc, err := nats.Connect(nats_url)
	if err != nil {
		log.Printf("nats Listener failed :%s", err.Error())
		return
	}
	defer nc.Close()

	// create an event  store
	opts := badger.DefaultOptions(badgerDir)
	db, _ := badger.Open(opts)
	store := &evs.Store{DB: db, Name: "BadgerEventStore"}
	defer store.Close()

	// subscribe to all inputs
	topic := prefix + ">"
	//nc.Subscribe(topic, messageHandler)
	// nc.Subscribe(topic, watcher.DefaultBoundMessageHandler(store))
	nc.Subscribe(topic, boundMessageHandler(store))

	// launch loop
	ctx, cancel := context.WithCancel(context.Background())

	// wait for cancel
	go func() {
		for {
			log.Printf("nats Listener enter loop (wait for cancel)")
			select {
			case <-ctx.Done(): // Done  this context is canceled -> exit
				fmt.Println("nats listener Exiting")
				return
			}
		}
	}()

	log.Println("wait for timer ....")
	time.Sleep(30 * time.Second)
	log.Println("send cancel")
	cancel()
	log.Println("exited")

	log.Println("start reading database")

	var e1, e2 evs.Event
	var t1, t2 time.Time

	//
	// select keys from a time range within window
	//
	//start := t0.Add(1 * time.Second)
	start := t0
	delay, _ := time.ParseDuration("40s")

	keys, err := store.WindowKeys(start, delay)
	if err != nil {
		log.Printf("no keys in time window")
	} else {
		log.Printf("keys collected: %d", len(keys))
		if len(keys) > 0 {
			log.Printf("first key: %s", keys[0])
			log.Printf("last key: %s", keys[len(keys)-1])
		}
	}

	b0, b1, err := evs.UlidTimeBoundaries(start, delay)
	if err != nil {
		t.Fail()
		return
	}

	//ctx := context.Background()
	eventChan, err := store.WindowRead(ctx, b0.String(), b1.String())

	if err != nil {
		log.Printf("WindowRead failed:%s", err.Error())
		t.Fail()
		return
	}

	var collected []evs.Event

	for ev := range eventChan {
		valid, reason := ev.IsValid()
		if valid {
			collected = append(collected, ev)
			//println("event: ", ev.ID)
		} else {
			println("error event :", reason)
			break
		}
	}

	if len(collected) > 0 {
		println("collected : ", len(collected))
		e1 = collected[0]
		t1, _ = e1.GetTimestamp()
		log.Printf("first ulid key : t= %s -> %s\n", e1.ID, t1)

		e2 = collected[len(collected)-1]
		t2, _ = e2.GetTimestamp()
		log.Printf("last  ulid key : t= %s -> %s\n", e2.ID, t2)

	} else {
		println("no event collected")
	}

}

// a simple message handler
func messageHandler(m *nats.Msg) {

	log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

}

// return a message handler bound to a event store
func boundMessageHandler(store *evs.Store) func(*nats.Msg) {

	return func(m *nats.Msg) {
		log.Printf("message handler bound to:%s\n", store.Name)
		log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

		// write message to db
		event, err := evs.NewTimestamp(m.Data, time.Time{})
		if err != nil {
			log.Printf("Cannot create to event store")
		} else {
			err := store.StoreEvent(event)
			if err != nil {
				log.Printf("Cannot write to event store")
			}
		}
	}
}
