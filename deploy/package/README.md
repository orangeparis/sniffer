# deploy sniffer

we deploy a zip containing standalone go executables

* sniffer   ( cmd/sniffer/sniffer.go)
* listener  ( cmd/listener/listener.go) for test purpose
* nats_server  a nats server ( fetch from official nats zip)

# unzip to /usr/local/bin

usage:

    # launch it
    nats_server &
    sleep 2
    sudo sniffer --name S1 --interface eth1 --nats nats://localhost:4222

    # to test it
    listener --nats nats://localhost:4222


