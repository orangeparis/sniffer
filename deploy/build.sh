# clean up
rm  ./package/sniffer.*
rm  ./package/*.zip
# install go dependencies
# go get github.com/spf13/viper

# compile 
export CGO_ENABLED=0
export GOARCH=amd64
# linux
export GOOS=linux
#go build -v -o ./package/sniffer-$GOOS-$GOARCH ../cmd/sniffer/sniffer.go
# windows
#export GOOS=windows
#go build -v -o ./infralis/infralis-$GOOS-$GOARCH.exe ../cmd/infralis/main.go
# add elements
# build the zip
cd package; zip -r sniffer.zip *
# compile to osx
export GOOS=darwin
go build -v -o ./sniffer-darwin-amd64 ../cmd/sniffer

# for alpine
#CC=$(which musl-gcc) go build --ldflags '-w -linkmode external -extldflags "-static"' server.go