package config

import (
	"fmt"

	"github.com/spf13/viper"
)

func init() {

	// config files
	viper.AddConfigPath("/etc/sniffer/")  // path to look for the config file in
	viper.AddConfigPath("$HOME/.sniffer") // call multiple times to add many search paths
	viper.AddConfigPath(".")              // optionally look for config in the working directory

	// Env vars
	viper.SetEnvPrefix("sniffer")

	// defaults

	// main
	viper.SetDefault("appName", "sniffer")
	viper.SetDefault("version", "0.1")

	// nats publisher
	//viper.SetDefault("nats.servers", []string{"nats://192.168.99.100:4222"})
	viper.SetDefault("nats.servers", []string{"nats://demo.nats.io:4222"})

	viper.SetDefault("nats.prefix", "sniffer.0.")

	// screen publisher
	viper.SetDefault("screen.verbose", true)

	// event store ( badger)
	viper.SetDefault("store.badger", "./badgerdb")

	// watcher
	viper.SetDefault("watcher.url", "localhost")
	viper.SetDefault("watcher.port", 8080)
	viper.SetDefault("watcher.prefix", "sniffer")

	// [prometheus]
	viper.SetDefault("prometheus.host", "prometheus:9090")

}

// Load : read the config files
func Load() {

	// read main "config.toml" file
	viper.SetConfigName("config") // name of config file (without extension)
	err := viper.ReadInConfig()   // Find and read the config file
	if err != nil {               // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %s", err))
	}
	// merge with "another.toml" file
	//viper.SetConfigName("another")
	//viper.MergeInConfig()

}

//
//    helpers
//

// Get : Get config key
func Get(key string) interface{} {
	return viper.Get(key)
}

// Set config key with value
func Set(key string, value interface{}) {
	viper.Set(key, value)
}

// GetString : get key as string
func GetString(key string) string {
	return viper.GetString(key)
}

// GetInt : get key as string
func GetInt(key string) int {
	return viper.GetInt(key)
}

// GetBool : get key as boolean
func GetBool(key string) bool {
	return viper.GetBool(key)
}

// GetList : get key as string
func GetList(key string) []string {
	return viper.GetStringSlice(key)
}
