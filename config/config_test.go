package config_test

import (
	"testing"

	"bitbucket.org/orangeparis/sniffer/config"
	"github.com/spf13/viper"
)

func TestConfig(t *testing.T) {

	config.Load()
	data := viper.AllSettings()
	_ = data

	version := config.GetString("version")
	_ = version

	nats_servers := config.GetList("nats.servers")
	_ = nats_servers

	screen_verbose := config.GetBool("screen.verbose")
	_ = screen_verbose

	println("done")

}
