package devicePlayer

import (
	"fmt"
	"strings"

	"bitbucket.org/orangeparis/sniffer"
	//"fmt"
	"log"
	"os"
	"time"

	"bitbucket.org/orangeparis/sniffer/filters"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

var (
//device       string = "en0"
//snapshot_len int32  = 1024
//promiscuous  bool   = false
//err          error
//timeout      time.Duration = 30 * time.Second
//handle       *pcap.Handle
)

var defaultBpfFilter string = "portrange 67-68 or  (vlan && portrange 67-68)"

// a redis pubsub publisher
type DevicePlayer struct {
	publisher    sniffer.Publisher //  a nats or screen publisher to send  message
	filter       *filters.Filter
	device       string        // interface device eg eth0
	snapshot_len int32         // len eg 1024
	promiscuous  bool          // default  false
	timeout      time.Duration // eg 30 * time.Second
	handle       *pcap.Handle
	counter      int
}

func NewDevicePlayer(device string, publisher sniffer.Publisher, bpfFilter string) (player *DevicePlayer, err error) {

	//var handle  *pcap.Handle

	log.Printf("create new Device player for interface \n")

	snapshot_len := int32(1024)
	promiscuous := true
	timeout := time.Duration(30 * time.Second)

	handle, err := pcap.OpenLive(device, snapshot_len, promiscuous, timeout)
	if err != nil {
		return player, err
	}

	// Set BPF filter
	if bpfFilter != "all" {
		if bpfFilter == "" {

			bpfFilter = defaultBpfFilter //"portrange 67-68 or  (vlan && portrange 67-68)"
		}
		err = handle.SetBPFFilter(bpfFilter)
		if err != nil {
			log.Printf(err.Error())
			return player, err
		}
		fmt.Printf("bpf filter: %s\n", bpfFilter)
	}

	//
	//hive := dnsbroker.NewHive(name,url)
	//publisher := dnsbroker.NewStore()
	filter := filters.NewFilter(publisher)

	//
	player = &DevicePlayer{
		filter:       filter,
		publisher:    publisher,
		device:       device,
		snapshot_len: snapshot_len,
		timeout:      timeout,
		handle:       handle,
	}

	return player, err
}

func (player *DevicePlayer) Close() {
	//
	//player.publisher.Close()
	player.handle.Close()
	log.Printf("Guardian closed: %d packets processed\n", player.counter)
	os.Exit(-1)

}

func (player *DevicePlayer) Start() {

	//uptimer, _ := uptime.NewUptimer(player.publisher, 5)
	//uptimer.Start()

	player.Handle()

}

func (player *DevicePlayer) Handle() {

	//log.Printf("Handle")
	// main loop

	log.Printf("entering main Handler loop\n")

	var packetId uint32

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(player.handle, player.handle.LinkType())

	packetId = 0 // (p.id )
	for p := range packetSource.Packets() {

		//spew.Dump(p)
		player.HandlePacket(p, packetId)
		player.counter++
		packetId++
		// tempo to slow down
		//time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from source \n")

}

func (player *DevicePlayer) HandlePacket(packet gopacket.Packet, id uint32) {

	//log.Printf("==================== PKT [%03d] \n", id)
	// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
	// fmt.Println(hex.Dump(payload.Data))
	// Decode a packet

	event, err := player.filter.Filter(packet, id)
	//println("dissector result:",err,"\n")
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "No decoder") {
			// skip error
			//println("skip")
		} else {
			println("filter error result:", err.Error(), "\n")
		}
	} else {
		// success we have a dns event
		//spew.Dump(event)
	}

	_ = event

	player.counter++
	// tempo to slow down
	//time.Sleep( 500 * time.Microsecond)
}
