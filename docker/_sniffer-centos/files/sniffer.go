package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"strings"

	"bitbucket.org/orangeparis/sniffer"
	"bitbucket.org/orangeparis/sniffer/devicePlayer"
	"bitbucket.org/orangeparis/sniffer/filePlayer"
	"bitbucket.org/orangeparis/sniffer/natspublisher"
	"bitbucket.org/orangeparis/sniffer/screenpublisher"
)

var (
	Version = "0.1"

	name = flag.String("name", "X", "name of the sniffer (eg 0 , will publish to sniffer.0.>)")

	networkInterface = flag.String("interface", "eth0", "network interface (eg eth0)")
	pcapFile         = flag.String("pcap", "", "pcap file to read (eg: sample.pcap)")

	nats   = flag.String("nats", "nats://demo.nats.io:4222", "nats server (eg nats://demo.nats.io:4222)")
	screen = flag.Bool("screen", false, "publish to screen")

	helper = `Scan a network interface OR a pcap file and send message to nats server OR screen
sample usage : sniffer --name s1 --interface eth0 --nats nats://<host>:4222
options:
--name string
	name of the sniffer (default "X") (eg 0 , will publish to sniffer.0.>) 

--interface string
    	network interface (default "eth0")
--pcap string
		pcap file to read (eg: sample.pcap)
		
--nats string
    	nats server (default "nats://demo.nats.io:4222")
--screen
    	publish to screen
`
)

func getPublisher() (publisher sniffer.Publisher, err error) {

	// return a publisher
	//  if --screen return a screen publisher
	//  else return a nats publisher

	// topic   eg sniffer.0.  sniffer.labo2.
	var topic string

	if strings.HasSuffix(*name, ".") {
		topic = fmt.Sprintf("sniffer.%s", *name)
	} else {
		topic = fmt.Sprintf("sniffer.%s.", *name)
	}

	if *screen == true {
		publisher = screenpublisher.NewScreenPublisher(topic)
		log.Println("sniffer : set Publisher to screen")

	} else {
		// a nats publisher
		natsURL := *nats
		publisher, err = natspublisher.NewNatsPublisher(topic, natsURL)
		log.Printf("sniffer : set Publisher to nats publisher at %s", natsURL)
	}
	log.Printf("sniffer : set root topic to: %s\n", topic)

	return publisher, err
}

func pcapMode(publisher sniffer.Publisher, pcap string) {

	//defer publisher.Close()

	player, err := filePlayer.NewPlayer(pcap, publisher)
	if err != nil {
		log.Printf("sniffer : error %s\n", err.Error())
		log.Fatal("sniffer : aborted.")
	}

	// launch the player loop
	log.Println("sniffer : start player")
	player.Start()

}

func interfaceMode(publisher sniffer.Publisher, device string) {

	player, err := devicePlayer.NewDevicePlayer(device, publisher)
	if err != nil {
		log.Printf("sniffer : error: %s\n", err.Error())
		log.Fatal("sniffer : aborted.")
	}

	// launch the player loop
	log.Println("sniffer : start player")
	player.Start()

}

func debugflag() {

}

func run() {

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, fmt.Sprintf("sniffer %s\n", Version))
		fmt.Fprintf(os.Stderr, helper)
	}

	flag.Parse()

	// get a publisher correspondinf to the flags ( screen or nats )
	publisher, err := getPublisher()
	if err != nil {
		log.Fatalf("sniffer: cannot get a publisher: %s\n", err.Error())
	}

	// find  mode ( interface or pcap )
	mode := "INTERFACE"
	if *pcapFile != "" {
		mode = "PCAP"
	}

	//log.Printf("sniffer: starting")
	switch mode {
	case "PCAP":
		// read from a pcap file
		log.Printf("sniffer: create a pcap reader with %s\n", *pcapFile)
		pcapMode(publisher, *pcapFile)

	default:
		// read from network interface (need to be root )
		log.Printf("sniffer : create a device reader with %s\n", *networkInterface)
		interfaceMode(publisher, *networkInterface)

	}
	log.Printf("sniffer : exiting")

}

func main() {
	run()
}
