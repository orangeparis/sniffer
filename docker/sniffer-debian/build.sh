
# get source
echo "copy sniffer.go"
cp ../../cmd/sniffer/*.go ./files

# build sniffer
echo " build sniffer"
docker build --force-rm --no-cache -t sniffer .


# env GOOS=linux GOARCH=amd64  CGO_ENABLED=1 /usr/local/go/bin/go build --ldflags '-extldflags "-static"' -o sniffer-linux-amd64 .