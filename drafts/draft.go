package main

import (
	"fmt"
	"log"
	"time"

	"github.com/nats-io/go-nats"
)

var (
	//nats_url = "nats://192.168.99.100:4222"
	nats_url = "nats://demo.nats.io:4222"
)

func main() {

	nc, err := nats.Connect(nats_url)
	if err != nil {
		log.Fatal(err.Error())

	}

	// Simple Async Subscriber
	nc.Subscribe("foo", func(m *nats.Msg) {
		fmt.Printf("Received a message: %s\n", string(m.Data))
	})

	// Simple Publisher
	nc.Publish("foo", []byte("Hello foo"))

	// Simple Sync Subscriber
	sub, err := nc.SubscribeSync("bar")
	if err != nil {
		log.Printf("error: %s", err.Error())
	}

	// Simple Publisher
	nc.Publish("bar", []byte("Hello bar"))

	m, err := sub.NextMsg(1 * time.Second)
	if err != nil {
		log.Printf("error waiting bar: %s", err.Error())
	} else {
		fmt.Printf("received:%v\n", m.Data)
	}

	//// Channel Subscriber
	//ch := make(chan *nats.Msg, 64)
	//sub, err = nc.ChanSubscribe("foo", ch)
	//if err != nil {
	//	log.Printf("error: %s",err.Error())
	//}
	//
	//msg := <- ch
	//_=msg

	// Unsubscribe
	sub.Unsubscribe()

	// Requests

	// Replies
	nc.Subscribe("help", func(m *nats.Msg) {
		nc.Publish(m.Reply, []byte("I can help!"))
	})

	msg, err := nc.Request("help", []byte("help me"), 1000*time.Millisecond)
	if err != nil {
		log.Printf("error: %s", err.Error())
	}
	fmt.Printf("received response to help :%v\n", string(msg.Data))

	time.Sleep(5 * time.Second)

	// Close connection
	nc, _ = nats.Connect(nats_url)
	nc.Close()

}
