# nats message structure

    type Msg struct {
      Subject string
      Reply   string
      Data    []byte
      Sub     *Subscription
      next    *Msg
      barrier *barrierInfo
    }