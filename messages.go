package _sniffer

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"log"
)

/*


	sniffer observe a network traffic and publish nats messages


	nats message have a subject and a body


	subjects :

	<publisher>.<device>.<event>.<type>

	publisher is the root of the subject , usualy the publisher id
	device is the id of the device usualy the mac address
	event is the name of the event
	type is a sub-type of event

	eg
	cx3.a4:7b:2c:e4:d7:77.dhcp.Request

	the sniffer named cx3 publish a dhcp request event for device : a4:7b:2c:e4:d7:77


	messages :

	are json key/value

	some usual keys
	DstIP:"172.20.123.119",
	DstMAC":"a4:7b:2c:e4:d7:74"
	"DstPort":67
	"SrcIP":"193.253.20.246"
	"SrcMAC":"12:08:e8:c0:1c:00"
	"SrcPort":68
	dhcpOp":"Request"
	"ip4Len":473,
	"lanId":2900
	"type":2048
	"udpLen":453

	"counter":1200

*/

// Msg  a struct to describe sniffer messages for gob encoding/decoding
type Msg struct {

	// main descripters
	EventName string // event type , eg dhcp , dns
	EventTag  string // event subtype  eg for Dhcp : [ request, reply]
	// some network info
	DstIP   string // "172.20.123.119"
	DstMAC  string // a4:7b:2c:e4:d7:74"
	DstPort int    // 67
	SrcIP   string // "193.253.20.246"
	SrcMAC  string // "12:08:e8:c0:1c:00"
	SrcPort int    // 68

	IP4Len uint16 // 473
	LanID  int    // 2900
	Type   int    // 2048
	UDPLen uint16 // 453

	VlanID   uint16 // vlan identifier
	VlanType string // vlan type

	// some generic properties
	Properties map[string]string // a map of string properties

	// some specific type
	Counter uint64 // 1200
	DhcpOp  string //Request"
}

// Message : The sniffer Message : event name , network data and more
// type Message struct {

// 	// main descripters
// 	EventName string // event type , eg dhcp , dns
// 	EventTag  string // event subtype  eg for Dhcp : [ request, reply]

// 	// some network info
// 	DstIP   string // "172.20.123.119"
// 	DstMAC  string // a4:7b:2c:e4:d7:74"
// 	DstPort int    // 67
// 	SrcIP   string // "193.253.20.246"
// 	SrcMAC  string // "12:08:e8:c0:1c:00"
// 	SrcPort int    // 68

// 	IP4Len uint16 // 473
// 	LanID  int    // 2900
// 	Type   int    // 2048
// 	UDPLen uint16 // 453

// 	VlanID   uint16 // vlan identifier
// 	VlanType string // vlan type

// 	// some generic properties
// 	Properties map[string]string // a map of properties

// 	// some specific type
// 	Counter uint64 // 1200
// 	DhcpOp  string //Request"

// }

//var network bytes.Buffer // Stand-in for the network.

// // MarshalBinary Encode message to gob
// func (m *Msg) MarshalBinary() (data []byte, err error) {

// 	var network bytes.Buffer // Stand-in for the network.

// 	// Create an encoder and send a value.
// 	enc := gob.NewEncoder(&network)
// 	err = enc.Encode(m)
// 	if err != nil {
// 		log.Fatal("encode:", err)
// 	}
// 	data = network.Bytes()
// 	// b := bytes.Buffer{}
// 	// e := gob.NewEncoder(&b)
// 	// err = e.Encode(&m)
// 	// return b.Bytes(), err

// 	return data, err

// }

// // UnmarshalBinary decode gob message
// func (m Message) UnmarshalBinary(data []byte) (err error) {

// 	// b := bytes.Buffer{}
// 	// b.Write(data)
// 	// d := gob.NewDecoder(&b)
// 	// err := d.Decode(&m)
// 	return err
// }

// GobEnbytecode : encode a sniffer message to a byte stream
func GobEncode(m Msg) (data []byte, err error) {

	var network bytes.Buffer // Stand-in for the network.
	// Create an encoder and send a value.
	enc := gob.NewEncoder(&network)
	err = enc.Encode(&m)
	if err != nil {
		log.Fatal("encode:", err)
	}
	data = network.Bytes()
	return data, err

}

// GobDecode : decode a byte steam to a sniffer message
func GobDecode(data []byte) (message Msg, err error) {

	var network bytes.Buffer // Stand-in for the network.
	network.Write(data)
	// Create a decoder and receive a value.
	dec := gob.NewDecoder(&network)
	var v Msg
	err = dec.Decode(&v)
	if err != nil {
		log.Fatal("decode:", err)
	}
	return v, err
}

// JSONEncode : encode a sniffer message to json
func JSONEncode(m Msg) (data []byte, err error) {
	return json.Marshal(m)
}

// JSONDecode : decode  a json byte streal to a sniffer message
func JSONDecode(data []byte, m Msg) (err error) {
	return json.Unmarshal(data, &m)
}
