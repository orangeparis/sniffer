package fastfilters

import (
	"encoding/json"
	"fmt"
	"log"

	//"time"

	"bitbucket.org/orangeparis/sniffer"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	//"strconv"
)

/*
	filter scan packets to detect specific traffic

	ptrocol and DstPort

	* dhcp      ( full support )
	* radius  1812,1813   ( minimal support )
	* ldp : 646   Label distribution protocol  ( no support)
	* snmp : 161 Simple network management protocol (no support )
	* multicast dns : 5353 , ( no support )


	Well Known Ports: 0 through 1023.
	Registered Ports: 1024 through 49151.
	Dynamic/Private : 49152 through 65535.

*/

type FilterEngine interface {
	Filter(rawPacket *gopacket.Packet) (event map[string]interface{}, err error)
}

type Filter struct {
	publisher     sniffer.MessagePublisher
	packetCounter uint64
}

var (
	macAddrFilter string = "02:ff:ff:e0:88:41"
	vlanFilter    int    = 2900
)

func NewFilter(publisher sniffer.MessagePublisher) (filter *Filter) {

	log.Printf("create new Filter\n")

	filter = &Filter{
		publisher: publisher,
	}

	return filter
}

// Filter : filters a packet : publish message and return an event map
func (f *Filter) Filter(packet gopacket.Packet, packetID uint32) (event map[string]interface{}, err error) {
	//
	//
	//

	//var err error
	message := sniffer.Msg{EventName: "Unknown"}
	layout := Layers{}

	event = make(map[string]interface{})
	event["_verdict"] = "PASS"
	event["udpLen"] = 0

	// create layer parser
	parser := gopacket.NewDecodingLayerParser(
		layers.LayerTypeEthernet,
		&layout.Dhcp,
		&layout.Vlan,
		&layout.Eth,
		&layout.Ip4,
		&layout.Ip6,
		&layout.Tcp,
		&layout.Udp,
		&layout.Dns,
		&layout.Body)

	var foundLayerTypes []gopacket.LayerType
	err = parser.DecodeLayers(packet.Data(), &foundLayerTypes)
	if err != nil {
		//log.Printf("[DecodeLayers error   ] %v %v", err, foundLayerTypes)
		return event, err
	}

	isDNS := false
	isDhcp := false

	for _, layer := range foundLayerTypes {
		switch layer {
		case layers.LayerTypeDHCPv4:
			isDhcp = true
			message.DhcpOp = layout.Dhcp.Operation.String()

		case layers.LayerTypeDot1Q:
			message.VlanID = layout.Vlan.VLANIdentifier
			message.VlanType = layout.Vlan.Type.String()

		case layers.LayerTypeEthernet:
			message.SrcMAC = layout.Eth.SrcMAC.String()
			message.DstMAC = layout.Eth.DstMAC.String()

		case layers.LayerTypeIPv4:
			message.SrcIP = layout.Ip4.SrcIP.String()
			message.DstIP = layout.Ip4.DstIP.String()
			message.IP4Len = layout.Ip4.Length

		case layers.LayerTypeIPv6:
			message.SrcIP = layout.Ip6.SrcIP.String()
			message.DstIP = layout.Ip6.DstIP.String()

		case layers.LayerTypeUDP:
			message.UDPLen = layout.Udp.Length
			message.SrcPort = int(layout.Udp.SrcPort)
			message.DstPort = int(layout.Udp.DstPort)

		case layers.LayerTypeDNS:
			isDNS = true
		}
	}

	// handle packet counter ( send a message every 100 packets)
	f.packetCounter++
	if f.packetCounter%100 == 0 {
		// every 100 packets received we send a message
		topic := fmt.Sprintf("stats.packet.counter")
		msg := sniffer.Msg{EventName: "Packet", EventTag: "Counter", Counter: f.packetCounter}
		f.publisher.Publish(topic, msg)
	}

	if isDhcp {
		// handle dhcp  sniffer.02:ff:ff:e0:88:41.dhcp.*
		err = HandleDhcp(f.publisher, message, &layout)
		//return event, err
	}

	if isDNS {
		// handle dns sniffer.02:ff:ff:e0:88:41.dns.*
		err = HandleDNS(f.publisher, message, &layout)
		//return event, err
	}

	if layout.Udp.SrcPort == 1812 || layout.Udp.SrcPort == 1813 || layout.Udp.DstPort == 1812 || layout.Udp.DstPort == 1813 {
		// handle radius sniffer.02:ff:ff:e0:88:41.radius.*
		err = HandleRadius(f.publisher, message, &layout)
		//return event, err
	}

	// prepare response event
	event = make(map[string]interface{})
	inrec, _ := json.Marshal(message)
	json.Unmarshal(inrec, &event)

	event["_verdict"] = "PASS"
	return event, err
}
