package fastfilters

import (
	"fmt"

	"bitbucket.org/orangeparis/sniffer"
	//"strconv"
)

// HandleRadius handle a dhcp packet
func HandleRadius(publisher sniffer.MessagePublisher, message sniffer.Msg, layout *Layers) (err error) {

	message.EventName = "radius"
	message.EventTag = "packet"

	// by default publish on srcMac channel: <SrcMac>.dhcp.<op>
	topic := fmt.Sprintf("%s.radius.%s", layout.Eth.SrcMAC.String(), message.EventTag)
	// switch op {
	// case "Request":
	// 	// assume it is a livebox request send a dhcp message   root.<srcMac>.dhcp.<op>
	// 	//topic = fmt.Sprintf("%s.dhcp.%s", eth.SrcMAC.String(), op)
	// case "Reply":
	// 	// assume it is a dhcp reply to the livebox send a dhcp message root.<DstMac>.dhcp.<op>
	// 	topic = fmt.Sprintf("%s.dhcp.Reply", layout.Eth.DstMAC.String())
	// default:
	// 	// unkwonw dhcp type ???? should not happen
	// 	log.Printf("dhcp filter: unknown type: %s", op)
	// }
	err = publisher.Publish(topic, message)
	return err
}
