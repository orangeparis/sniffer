package natspublisher

import (
	"bytes"
	"encoding/gob"
	"log"

	"bitbucket.org/orangeparis/sniffer"
)

/*

	a nats publisher to publisher sniffer.Message ( gob )


*/

// GobNatsPublisher : a publisher to publish gob sniffr message to nats
type GobNatsPublisher struct {
	*NatsPublisher
}

// NewGobNatsPublisher : create a screen publisher with a root topic
func NewGobNatsPublisher(topic string, server string) (publisher GobNatsPublisher, err error) {

	p, err := NewNatsPublisher(topic, server)
	if err != nil {
		return publisher, err
	}
	publisher = GobNatsPublisher{p}
	return publisher, err
}

// Publish go sniffer msg to nats server
func (p GobNatsPublisher) Publish(topic string, message sniffer.Msg) (err error) {

	// compute full topic
	topic = p.Topic + topic

	// gob serialize message
	var buffer bytes.Buffer
	enc := gob.NewEncoder(&buffer)
	err = enc.Encode(&message)
	if err != nil {
		log.Printf("GobNatsPublisher encoding error: %s", err.Error())
	}
	data := buffer.Bytes()

	// publish to nats
	err = p.Conn.Publish(topic, data)
	return err

}
