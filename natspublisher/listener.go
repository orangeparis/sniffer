package natspublisher

import (
	"context"
	"log"

	"github.com/nats-io/go-nats"
)

// Listener : universal listener
// usage :
// 		ctx, cancel := context.WithCancel(context.Background())
// 		go Listener( ctx,"url","root")
//		...
//		cancel()
func Listener(ctx context.Context, natsUrl string, topic string) (err error) {

	if topic == "" {
		// default topic : all
		topic = ">"
	} else {
		// modify topic to catch all from root topic
		topic = topic + ".>"
	}
	nc, err := nats.Connect(natsUrl)
	if err != nil {
		log.Printf("nats Listener failed :%s\n", err.Error())
		return err
	}

	defer nc.Close()

	// subscribe to all inputs
	nc.Subscribe(topic, func(m *nats.Msg) {
		//log.Printf("Listener catch a message: %s on  subject: %s\n", string(m.Data), m.Subject)
		log.Printf("Listener caught on [" + m.Subject + "] the message:\n" + string(m.Data) + "\n")

	})

	// wait for cancel
	for {
		log.Printf("nats Listener enter loop (wait for cancel)")
		select {
		case <-ctx.Done(): // Done  this context is canceled -> exit
			log.Println("nats listener Exiting")
			return err
		}
	}

}
