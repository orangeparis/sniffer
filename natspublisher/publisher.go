package natspublisher

import (
	"fmt"
	"log"

	"github.com/nats-io/go-nats"
)

// NatsPublisher : a publisher to publish to screen ( for debug )
type NatsPublisher struct {
	*nats.Conn
	Topic   string // root topic to indentify publisher
	Server  string // server or list of nats servers
	Counter uint64 // number of messages published
}

// NewNatsPublisher : create a screen publisher with a root topic
func NewNatsPublisher(topic string, server string) (*NatsPublisher, error) {
	if topic == "" {
		topic = "root."
	}
	p := &NatsPublisher{Topic: topic, Server: server}
	conn, err := nats.Connect(p.Server)
	if err != nil {
		log.Printf("cannot open nats connection: %s", err.Error())
		return p, err
	}
	p.Conn = conn
	msg := fmt.Sprintf("Nats publisher connected with : %s\n", p.Server)
	log.Println(msg)

	return p, err
}

// func (p NatsPublisher) Connect() (err error) {

// 	// connect to nats server
// 	p.Conn, err = nats.Connect(p.Server)
// 	if err != nil {
// 		log.Printf("cannot open nats connection: %s", err.Error())
// 		return err
// 	}
// 	msg := fmt.Sprintf("Nats publisher connected with : %s\n", p.Server)
// 	log.Println(msg)

// 	return err
// }

// Close : close nats connection
func (p *NatsPublisher) Close() {

	p.Conn.Close()
	p.Conn = nil
	log.Printf("Nats publisher closed\n")

}

// Publish msg to nats server
func (p *NatsPublisher) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	// publish to nats
	err = p.Conn.Publish(subject, message)
	if err == nil {
		// increment counter
		p.Counter += 1
	}
	//log.Printf("natspublisher: publish to [" + topic + "] the message:\n" + string(message) + "\n")
	return err

}
