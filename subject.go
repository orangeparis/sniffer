package _sniffer

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

//
// helper for handling sniffer subject :EmitterType.EmitterName.DeviceId.MsgType.MsgTag.Sequence
//

var SubjectSchema string = "EmitterType.EmitterName.DeviceId.MsgType.MsgTag.Sequence"

// Subject : the sniffer subject
type Subject struct {
	EmitterType string // sniffer
	EmitterName string // name of emitter eg : s1

	DeviceId string // usualy a mac address

	MsgType  string // eg dhcp
	MsgTag   string // a tag , can be sub type eg request/reply
	Sequence int
}

// NewReceivedSubject : Create a subject object from a received message subject
func NewReceivedSubject(subject string) (s *Subject, err error) {

	s = &Subject{}
	err = s.Parse(subject)
	return s, err
}

func (s *Subject) String() string {
	if s.Sequence >= 0 {
		// got a valid sequence
		return fmt.Sprintf("%s.%s.%s.%s.%s.%d",
			s.EmitterType, s.EmitterName, s.DeviceId, s.MsgType, s.MsgTag, s.Sequence)
	}
	// no sequence:
	return fmt.Sprintf("%s.%s.%s.%s.%s",
		s.EmitterType, s.EmitterName, s.DeviceId, s.MsgType, s.MsgTag)

}

func (s *Subject) Parse(subject string) (err error) {
	parts := strings.Split(subject, ".")
	if len(parts) < 5 {
		// bad sniffer subject format
		err = errors.New("INVALID_FORMAT")
		return err
	}
	s.EmitterType = parts[0]
	s.EmitterName = parts[1]
	s.DeviceId = parts[2]
	s.MsgType = parts[3]
	s.MsgTag = parts[4]
	s.Sequence = -1 // means we dont yet have a sequence indicator

	if len(parts) > 5 {
		// set sequence indicator
		seq, err := strconv.Atoi(parts[5])
		if err != nil {
			err = errors.New("INVALID_FORMAT:SEQUENCE")
			return err
		}
		s.Sequence = seq
	}
	return err
}
