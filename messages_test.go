package _sniffer_test

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"log"
	"testing"

	"bitbucket.org/orangeparis/sniffer"
)

type Vector struct {
	X, Y, Z int
}

func TestGob(t *testing.T) {

	var network bytes.Buffer // Stand-in for the network.

	// Create an encoder and send a value.
	enc := gob.NewEncoder(&network)
	err := enc.Encode(Vector{3, 4, 5})
	if err != nil {
		log.Fatal("encode:", err)
	}

	// Create a decoder and receive a value.
	dec := gob.NewDecoder(&network)
	var v Vector
	err = dec.Decode(&v)
	if err != nil {
		log.Fatal("decode:", err)
	}
	fmt.Println(v)
	if v.X != 3 {
		t.Fail()
	}
}

func TestMarshallingMessage(t *testing.T) {

	//gob.Register(sniffer.Message{})

	//msg1 := sniffer.Message{EventName: "MyEvent", Counter: 568}
	props := make(map[string]string)
	props["status"] = "OK"

	msg1 := sniffer.Msg{EventName: "MyEvent", Counter: 123, Properties: props}
	b1, err := sniffer.GobEncode(msg1)
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}

	msg2, err := sniffer.GobDecode(b1)
	if err != nil {
		log.Println(err.Error())
		t.Fail()
		return
	}
	if msg1.EventName != msg2.EventName {
		t.Fail()
		return
	}
	if msg2.Properties["status"] != msg1.Properties["status"] {
		t.Fail()
		return
	}
	//println(msg1 == msg2)
}
