package _sniffer_test

import (
	"log"
	"testing"

	"bitbucket.org/orangeparis/sniffer"
)

func TestMacVendors(t *testing.T) {

	goods := []string{
		"c0:d0:44:d8:5c:90", " 90:4D:4a:d8:5c:90 ",
	}

	for _, mac := range goods {
		vendor, ok := sniffer.GetVendor(mac)
		if ok != true {
			log.Printf("failed to get vendor from %s\n", mac)
			t.Fail()
		} else {
			log.Printf("vendor from %s is %s\n", mac, vendor)
		}
	}

}
