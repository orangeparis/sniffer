package netfilterPlayer

import (
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/kung-foo/freki/netfilter"
)

var blankEthHeader = []byte{
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x08, 0x00,
}

func GopacketFromNetfilterRawPacket(rawPacket *netfilter.RawPacket) (packet gopacket.Packet) {

	// transform a netfilter raw packet into gopacket packet
	// just add an ethernet header ( because netfilter squeeze it)
	// see: https://github.com/kung-foo/freki/blob/master/freki.go

	buffer := append(blankEthHeader, rawPacket.Data...)
	packet = gopacket.NewPacket(
		buffer,
		layers.LayerTypeIPv4,
		gopacket.DecodeOptions{Lazy: false, NoCopy: true},
	)

	return packet

}
