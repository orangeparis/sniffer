package netfilterPlayer

import (
	"log"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/orangeparis/sniffer"
	"bitbucket.org/orangeparis/sniffer/filters"
	"github.com/kung-foo/freki/netfilter"
)

func onErrorExit(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func onInterruptSignal(fn func()) {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		fn()
	}()
}

// iptables -A INPUT --dport dns -j NFQUEUE --queue-num 0
// iptables -A OUTPUT --sport dns -j NFQUEUE --queue-num 0

// a redis pubsub publisher
type NetFilterPlayer struct {
	publisher sniffer.Publisher // not yet implemented
	filter    *filters.Filter

	nfqueue *netfilter.Queue
	counter int
}

func NewNetFilterPlayer(publisher sniffer.Publisher) (player *NetFilterPlayer, err error) {

	log.Printf("create new netfilter player\n")

	nfqueue, err := netfilter.New(0, 100, netfilter.NF_DEFAULT_PACKET_SIZE)
	onErrorExit(err)

	//publisher := "no-publisher"
	filter := filters.NewFilter(publisher)

	player = &NetFilterPlayer{

		publisher: publisher,
		filter:    filter,

		nfqueue: nfqueue,
	}

	return player, err
}

func (player *NetFilterPlayer) Close() {
	//

	player.nfqueue.Close()
	//player.publisher.Close()
	log.Printf("player closed: %d packets processed\n", player.counter)
	os.Exit(-1)
}

func (player *NetFilterPlayer) Start() {

	onInterruptSignal(func() {
		player.Close()
	})

	// start nfqueue
	log.Printf("starting nfqueue\n")
	go player.nfqueue.Run()
	log.Printf("nfqueue started\n")

	player.HandlePackets()

}

func (player *NetFilterPlayer) HandlePackets() {

	log.Printf("Handle Dns")
	// main loop
	pChan := player.nfqueue.Packets()

	log.Printf("entering main HandleDns loop\n")

	for p := range pChan {
		//log.Printf("packet received here\n")

		//spew.Dump(p)
		// p is a netfilter rawPacket
		// p.ID is  netfilter packet id to set verdict on
		// p.Data is the raw content (ip layer) of the received packet )

		player.HandlePacket(p)
		player.counter++

		// tempo to slow down
		time.Sleep(1 * time.Millisecond)

	}

}

func (player *NetFilterPlayer) HandlePacket(rawpacket *netfilter.RawPacket) {

	packetId := rawpacket.ID

	//log.Printf("==================== PKT [%03d] \n", packetId)
	// fmt.Printf("------------------------------------------------------------------------\n id: %d\n", payload.Id)
	// fmt.Println(hex.Dump(payload.Data))
	// Decode a packet

	// transform the netfilter rawpacket to a gopacket for filter
	packet := GopacketFromNetfilterRawPacket(rawpacket)

	event, err := player.filter.Filter(packet, packetId)
	//println("dissector result:",err,"\n")
	if err != nil {
		println("filter error result:", err, "\n")
	} else {
		// success we have a dns event
		//spew.Dump(event)
	}

	if event["_verdict"] == "DROP" {
		log.Printf("==================== DROP PACKET: [%03d]", packetId)
		player.nfqueue.SetVerdict(rawpacket, netfilter.NF_DROP)
	} else {
		log.Printf("===================== ACCEPT PACKET: [%03d]", packetId)
		player.nfqueue.SetVerdict(rawpacket, netfilter.NF_ACCEPT)
	}

}
