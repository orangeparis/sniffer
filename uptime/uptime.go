package uptime

import (
	"fmt"
	"strconv"
	"time"

	"bitbucket.org/orangeparis/sniffer"
)

/*
	once started emit a message { "counter": 100 }
	on subject sniffer.<name>.stats.uptime



*/

// internal value of uptime
var count float64

// Timer publish uptime message for a sniffer
type Timer struct {
	publisher sniffer.Publisher //  a nats or screen publisher to send  message
	Device    string            // type of device  : eg sniffer
	Name      string            // name of the device : eg S1
	Period    time.Duration     // eg 10 * time.Second

	counter float64   // number of second since started
	started time.Time //
}

// NewUptimer : create an uptimer
func NewUptimer(publisher sniffer.Publisher, period int) (timer *Timer, err error) {

	if period == 0 {
		period = 10 // send every 10 seconds
	}
	x := strconv.Itoa(period)
	_ = x
	speriod, err := time.ParseDuration(strconv.Itoa(period) + "s")
	if err != nil {
		speriod, _ = time.ParseDuration("10s")
	}
	timer = &Timer{
		publisher: publisher,
		Period:    speriod,
		counter:   0,
		started:   time.Now(),
	}
	return timer, err
}

// Start the timer
func (t *Timer) Start() {

	go func() {

		for {
			now := time.Now()
			elapsed := now.Sub(t.started)
			message := fmt.Sprintf(`{"counter":%d}`, int(elapsed.Seconds()))

			topic := "stats.uptime"
			t.publisher.Publish(topic, []byte(message))

			time.Sleep(t.Period)
		}
	}()

}
