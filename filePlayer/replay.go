package filePlayer

/*


	filter dns packets comming from a pcap file

*/

import (
	"fmt"
	"log"
	"strings"

	//"os/signal"

	"time"

	"bitbucket.org/orangeparis/sniffer"
	"bitbucket.org/orangeparis/sniffer/filters"
	"bitbucket.org/orangeparis/sniffer/screenpublisher"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

var defaultBpfFilter string = "portrange 67-68 or  (vlan && portrange 67-68)"

// a redis pubsub publisher
type Player struct {
	publisher sniffer.Publisher //  a nats or screen publisher to send  message
	filter    *filters.Filter
	handle    *pcap.Handle
	counter   int
}

func NewPlayer(pcapFile string, publisher sniffer.Publisher, bpfFilter string) (player *Player, err error) {

	var handle *pcap.Handle

	if publisher == nil {
		publisher = screenpublisher.NewScreenPublisher("default.")
	}

	log.Printf("create new file player\n")

	player = &Player{}

	// Open file instead of device
	handle, err = pcap.OpenOffline(pcapFile)
	if err != nil {
		return player, err
	}

	// Set BPF filter if not "all"
	if bpfFilter != "all" {
		if bpfFilter == "" {
			// set default bpf filter : "portrange 67-68 or  (vlan && portrange 67-68)
			bpfFilter = defaultBpfFilter
		}
		err = handle.SetBPFFilter(bpfFilter)
		if err != nil {
			log.Printf(err.Error())
			return player, err
		}
		fmt.Printf("bpf filter: %s\n", bpfFilter)
	}

	// set custom filter
	filter := filters.NewFilter(publisher)

	player = &Player{
		filter:    filter,
		publisher: publisher,
		handle:    handle,
		counter:   0,
	}

	return player, err
}

func (player *Player) Close() {
	//
	//player.publisher.Close()
	player.handle.Close()
	log.Printf("sniffer closed: %d packets processed\n", player.counter)
	return

}

func (player *Player) Start() {

	//uptimer, _ := uptime.NewUptimer(player.publisher, 5)
	//uptimer.Start()

	player.HandlePackets()

}

func (player *Player) HandlePackets() {

	log.Println("Handle Packets")
	// main loop

	log.Printf("entering main HandlePackets loop\n")

	var packetId uint32

	// Loop through packets in file
	packetSource := gopacket.NewPacketSource(player.handle, player.handle.LinkType())

	packetId = 0 // (p.id )
	for p := range packetSource.Packets() {

		//spew.Dump(p)

		player.HandlePacket(p, packetId)
		player.counter++
		packetId++
		// tempo to slow down
		//time.Sleep(1 * time.Millisecond)
	}

	log.Printf("no more packets from source , total: %d\n", player.counter)

}

func (player *Player) HandlePacket(packet gopacket.Packet, id uint32) {

	//log.Printf("==================== PKT [%03d] \n", id)

	event, err := player.filter.Filter(packet, id)
	_ = event
	if err != nil {
		e := err.Error()
		if strings.HasPrefix(e, "No decoder") {
			// skip error
			//println("skip")
		} else {
			println("filter error result:", err.Error(), "\n")
		}
	} else {
		// success
		//spew.Dump(event)
	}
	// tempo to slow down
	time.Sleep(1 * time.Millisecond)
}
