package filePlayer_test

import (
	"log"
	"testing"
	"time"

	"bitbucket.org/orangeparis/sniffer/filePlayer"
	"bitbucket.org/orangeparis/sniffer/screenpublisher"
	"github.com/google/gopacket/pcap"
)

var (
	//pcapFile string = "../fixtures/ines_dhcp.pcap"
	//pcapFile string = "../fixtures/RADIUS.cap"
	pcapFile string = "../fixtures/lbreboot_ne.pcap"

	handle *pcap.Handle
	err    error
)

func TestPlayer(t *testing.T) {

	publisher := screenpublisher.NewScreenPublisher("sniffer.pcap.")

	player, err := filePlayer.NewPlayer(pcapFile, publisher, "")
	if err != nil {
		log.Printf("error: %s", err.Error())
		return
	}
	player.Start()

	time.Sleep(5 * time.Second)

	log.Printf("publisher has published : %d", publisher.Counter)

	return

}
