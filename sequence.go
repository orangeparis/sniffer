package _sniffer

//
// Sequence
//

type Sequence struct {
	Next int
}

// Feed : proceed a sequence (status:  OK/RESET/AHEAD/LATE/NONE and increment next)
func (s *Sequence) Feed(seq int) (status string) {

	status = ""
	if seq == s.Next {
		// The nominal case
		status = "OK"
		// we are in sequence
	} else {
		// we are out sequence
		if seq == 0 {
			// publisher explicitly start a new sequence
			status = "RESET"
			// sequence has been reset by publisher ( cannot now if we missed messages
		} else {
			if seq < 0 {
				// we dont have a sequence
				status = "NONE"
			} else {
				// the out sequence cases , not a reset
				if seq > s.Next {
					status = "AHEAD"
					// the sequence received is ahead of expected, we may ave missed message(s)
				} else {
					status = "LATE"
					// the sequence received is before of expected, we get a later message ( maybe a missed one)
				}
			}
		}
	}
	// in all case except -1 (no sequence), next is received sequence +1
	if seq >= 0 {
		s.Next = seq + 1
	}
	return status
}

// set the next sequence
func (s *Sequence) Set(seq int) {
	s.Next = seq
}
