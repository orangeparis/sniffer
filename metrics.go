package _sniffer

/*

	define prometheus metrics for sniffer

	sniffer subject :  <sniffer_name>


	main labels:



*/

import (

	//"fmt"
	//"time"

	//"runtime"

	"github.com/prometheus/client_golang/prometheus"
)

// InitMetrics : declare metrics
func InitMetrics() {

	prometheus.MustRegister(EventCounter)
	prometheus.MustRegister(PacketCounter)
	prometheus.MustRegister(Uptime)

	prometheus.MustRegister(dhcpRequestCounter)
	prometheus.MustRegister(dhcpReplyCounter)
	prometheus.MustRegister(snifferStatus)
}

// EventCounter : generic event counter ( <sniffer>.<device>.<event>.<tag>)
var EventCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "event",
		Help: "number of events",
	},
	[]string{"sniffer", "device", "event", "type"},
)

// PacketCounter : generic packet counter
var PacketCounter = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "packetCounter",
		Help: "count of packets treated by the source",
	},
	[]string{"source"},
)

// Uptime : uptime in seconds of the source
var Uptime = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "uptime",
		Help: "count of seconds since sniffer start",
	},
	[]string{"device", "name"}, // eg device=sniffer , name=S1
)

//
//  specific events for dhcpRequest , dhcpReply
//

var dhcpRequestCounter = prometheus.NewCounterVec(
	prometheus.CounterOpts{
		Name: "dhcpRequest",
		Help: "number of dhcp request sent",
	},
	[]string{"device"},
)

var dhcpReplyCounter = prometheus.NewCounterVec(

	prometheus.CounterOpts{
		Name: "dhcpReply",
		Help: "number of dhcp  replies received",
	},
	[]string{"device"},
)

// status of the sniffer
// 0:
var snifferStatus = prometheus.NewGaugeVec(
	prometheus.GaugeOpts{
		Name: "status",
		Help: "status of the sniffer",
	},
	[]string{"sniffer"},
)

//
// metrics recorders ( helpers to reccord metrics)
//

// IncrementEventCounter : generic event counter
func IncrementEventCounter(sniffer, device, event, etype string) {
	EventCounter.With(
		prometheus.Labels{
			"sniffer": sniffer, "device": device, "event": event, "type": etype,
		}).Inc()
}

// RecordPacketCounter : number of packets treated by source
func RecordPacketCounter(count float64, source string) {
	PacketCounter.With(prometheus.Labels{"source": source}).Set(count)
}

// RecordUptime : number of packets treated by source
func RecordUptime(count float64, device string, source string) {
	PacketCounter.With(prometheus.Labels{"device": device, "source": source}).Set(count)
}

// IncrementDhcpRequest : increment the dhcp request counter of a device
func IncrementDhcpRequest(device string) {
	dhcpRequestCounter.With(prometheus.Labels{"device": device}).Inc()
}

// IncrementDhcpReply increment the dhcp reply counter of a device
func IncrementDhcpReply(device string) {
	dhcpReplyCounter.With(prometheus.Labels{"device": device}).Inc()
}

// RecordSnifferStatus  0: initialized , 1 running
func RecordSnifferStatus(status float64, sniffer string) {
	snifferStatus.With(prometheus.Labels{"sniffer": sniffer}).Set(status)
}
