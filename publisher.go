package _sniffer

// Publisher a generic data publisher ( []bytes )
type Publisher interface {
	Publish(topic string, messsage []byte) error
}

// MessagePublisher a generic sniffer message publisher
type MessagePublisher interface {
	Publish(topic string, messsage Msg) error
}
