# install nats from zip


> curl -L https://github.com/nats-io/nats-server/releases/download/v2.0.0/nats-server-v2.0.0-linux-amd64.zip -o nats-server.zip

> unzip nats-server.zip -d nats-server
Archive:  nats-server.zip
   creating: nats-server-v2.0.0-darwin-amd64/
  inflating: nats-server-v2.0.0-darwin-amd64/README.md
  inflating: nats-server-v2.0.0-darwin-amd64/LICENSE
  inflating: nats-server-v2.0.0darwin-amd64/nats-server

> cp nats-server-v2.0.0darwin-amd64/nats-server /usr/local/bin