package livebox

// LiveboxVendors : list of livebox s vendors
var LiveboxVendors = []string{
	"SAGEMCOM",
	"SERCOM",
}

// MacVendor some vendors for MacAddress
var MacVendor = map[string]string{
	"C0D044": "SAGEMCOM",
	"904D4A": "SAGEMCOM",
	"A01B29": "SAGEMCOM",
	"84A1D1": "SAGEMCOM",
	"7894B4": "SERCOM",

	"A47B2C": "NOKIA",
	"3C6104": "JUNIPER",
	"00133B": "Speed Dragon Multimedia Limited",
	"005056": "VMWARE",
}
