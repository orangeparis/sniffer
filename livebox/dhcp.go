package livebox

import "errors"

/*

	helper to handle dhcp



*/

// DHCPSubOptions helper to handle dhcp suboption
type DHCPSubOptions struct {
	Type   byte   // dhcp option code
	Length uint8  // dhcp option length
	Data   []byte // dhcp data of option ( sub options)

	index uint8 // current offset in option navigation
}

// Next : return next generic sub option
func (o *DHCPSubOptions) Next() (code uint8, data []byte, err error) {

	// check for the end of option
	if o.index >= o.Length {
		err = errors.New("EOF")
		return code, data, err
	}

	// proceed sub option
	code = uint8(o.Data[o.index])
	o.index++
	length := uint8(o.Data[o.index])
	o.index++

	if length > 0 {
		data = o.Data[o.index : o.index+length]
		o.index = o.index + length
	}
	return code, data, err
}
