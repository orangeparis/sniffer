package livebox_test

import (
	"testing"

	"bitbucket.org/orangeparis/sniffer/livebox"
)

func TestDHCPSubOptions(t *testing.T) {

	// simulate option 82 with a CircuitID and a null RemoteID
	type1 := uint8(82)
	length1 := uint8(31)
	data1 := []byte{1, 27, 71, 80, 79, 78, 52, 56, 48, 33, 48, 33, 70, 84, 84, 33, 49, 47, 48, 47, 57, 47, 48, 47, 49, 58, 56, 53, 50, 2, 0}

	opt := livebox.DHCPSubOptions{Type: type1, Length: length1, Data: data1}

	code, data, err := opt.Next()
	if err != nil {
		t.Fail()
	}
	_ = opt
	_ = code
	println(string(data))
	// GPON480!0!FTT!1/0/9/0/1:852
	if string(data) != "GPON480!0!FTT!1/0/9/0/1:852" {
		t.Fail()
	}

	code, data, err = opt.Next()
	if err != nil {
		t.Fail()
	}

	code, data, err = opt.Next()
	if err == nil {
		t.Fail()
	}

}
