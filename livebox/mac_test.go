package livebox_test

import (
	"log"
	"testing"

	"bitbucket.org/orangeparis/sniffer/livebox"
)

func TestMacVendors(t *testing.T) {

	goods := []string{
		"c0:d0:44:d8:5c:90", " 90:4D:4a:d8:5c:90 ",
	}

	for _, mac := range goods {
		vendor, ok := livebox.GetVendor(mac)
		if ok != true {
			log.Printf("failed to get vendor from %s\n", mac)
			t.Fail()
		} else {
			log.Printf("vendor from %s is %s\n", mac, vendor)
		}
	}

}

// check macaddress belongs to a livebox
func TestIsLiveboxPositive(t *testing.T) {

	goods := []string{
		"c0:d0:44:d8:5c:90", " 90:4D:4a:d8:5c:90 ",
	}

	for _, mac := range goods {
		isLivebox := livebox.IsLivebox(mac)
		if isLivebox != true {
			t.Fail()
		}
	}

}

// check macaddress does not belong to a livebox
func TestIsLiveboxNegative(t *testing.T) {

	data := []string{
		"A4:7B:2C:d8:5c:90", "02:ff:ff:e0:88:41",
	}

	for _, mac := range data {
		isLivebox := livebox.IsLivebox(mac)
		if isLivebox == true {
			t.Fail()
		}
	}

}
