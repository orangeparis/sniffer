package livebox

import "strings"

/*


	Some code to handle macaddress and vendors


*/

// GetVendor return the Vendor for the mac address
func GetVendor(mac string) (vendor string, ok bool) {

	// canonical vendor info from mac address
	v := strings.TrimSpace(mac)
	v = strings.ReplaceAll(v, ":", "")
	v = strings.ToUpper(v)
	if len(v) >= 6 {
		v = v[:6]
		vendor, ok = MacVendor[v]
	} else {
		ok = false
	}
	return vendor, ok
}

// IsLivebox : is this mac addrress belongs to a livebox ( "SAGEMCOM" or "SERCOM")
func IsLivebox(mac string) bool {
	vendor, ok := GetVendor(mac)
	if ok {
		for _, v := range LiveboxVendors {
			if vendor == v {
				return true
			}
		}
	}
	return false

}
