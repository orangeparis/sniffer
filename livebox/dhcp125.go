package livebox

/*
	handle custom orange dhcp option 125


*/

// DHCPSubOpt125 represents a DHCP sub option of orange 125 option
type DHCPSubOpt125 byte

// Constants for the DHCPOpt options.
const (
	DHCPOpt                            = 0
	AutoFallBack         DHCPSubOpt125 = 1
	ForcedPPP            DHCPSubOpt125 = 2
	IPTVRunningInterface DHCPSubOpt125 = 2
)

/*
if o.Type == 125 {
	// orange option livebox 125  AutoFallBack and ForcedPPP
	data := o.Data
	A := data[7]
	T := data[8]
	P := data[9]
	var CC = [2]byte{data[10], data[11]}
	if A == 1 {
		event["AutoFallBack"] = true
	} else {
		event["AutoFallBack"] = false
	}
	if P == 1 {
		event["ForcedPPP"] = true
	} else {
		event["ForcedPPP"] = false
	}
	event["IPTVRunningInterface"] = fmt.Sprintf("%#v", T)
	_ = T
	_ = CC

}
*/
