package livebox

/*
	handle dhcp option 82 ( standard but not covered by golang layers library)

*/

// DHCPSubOpt82 represents a DHCP sub option of dhcp option 82
type DHCPSubOpt82 byte

// Constants for the DHCPSubOpt82 options.
const (
	DHCPCircuitID = 1
	DHCPRemoteID  = 2
)

// String returns a string version of a DHCPSubOpt.
func (o DHCPSubOpt82) String() string {
	switch o {
	case DHCPCircuitID:
		return "DHCPCircuitID"
	case DHCPRemoteID:
		return "DHCPRemoteID"
	default:
		return "Unknown"
	}
}

/*
if o.Type == 82 {
	// generic option: DHCP Relay Agent Information Option
	// Ciruit-ID , Remote-ID , Vendor-ID

	relayAgentInformation := "yes"
	_ = relayAgentInformation
	offset := 0
	subOp := int(o.Data[offset])
	offset++
	l := int(o.Data[offset])
	offset++
	if l > 0 {
		data := o.Data[offset : l+offset]
		event["DHCPCircuitID"] = string(data)
		//println(string(data))
		_ = subOp
		_ = data
		// next
		offset += l
		if offset < int(o.Length) {
			subOp2 := int(o.Data[offset])
			offset++
			l2 := int(o.Data[offset])
			offset++
			if l2 > 0 {
				data2 := o.Data[offset : l2+offset]
				event["DHCPRemoteID"] = string(data2)
				//println(string(data2))
				offset += l2
				_ = data2
				_ = subOp2
			}
		}
	}
*/
